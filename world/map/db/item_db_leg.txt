// Name and Label MUST NOT exceed 23 characters (as marked)
//ID,   Name___________________,        Type,   Price,      Sell,       Weight,     ATK,    DEF,    Range,  Mbonus, Slot,   Gender, Loc,        wLV,    eLV,    View,   {UseScript},                                                            {EquipScript}
586,    ShortDeAlgodao,             5,      1000,       500,        15,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
610,    ShortJeans,                 5,      2000,       1000,       25,     0,      4,      0,      0,      0,      2,      1,      0,      30,     0,      {},     {}
632,    SaiaDeAlgodao,              5,      1000,       500,        10,     0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
642,    CalcaDeCouroDeCobra,        5,      2000,       1000,       60,     0,      6,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {}
731,    CalcaDeAssassino,           5,      10000,      3000,       20,     0,      6,      0,      0,      0,      2,      1,      0,      95,     0,      {},     {bonus bAgi, 2;}
768,    CalcaDeTerranite,           5,      2000,       1000,       15,     0,      5,      0,      0,      0,      2,      1,      0,      40,     0,      {},     {bonus bMdef, 10;}
2100,   SaiaVermelha,               5,      1000,       500,        20,     0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2101,   SaiaVerde,                  5,      1000,       500,        20,     0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2102,   SaiaAzulEscura,             5,      1000,       500,        20,     0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2103,   SaiaAmarela,                5,      1000,       500,        20,     0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2104,   SaiaAzulClara,              5,      1000,       500,        20,     0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2105,   SaiaRosa,                   5,      1000,       500,        20,     0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2106,   SaiaPreta,                  5,      1000,       500,        20,     0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2107,   SaiaLaranja,                5,      1000,       500,        20,     0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2108,   SaiaRoxa,                   5,      1000,       500,        20,     0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2109,   SaiaVerdeEscura,            5,      1000,       500,        20,     0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2110,   ShortVermelho,              5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2111,   ShortVerde,                 5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2112,   ShortAzulEscuro,            5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2113,   ShortAmarelo,               5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2114,   ShortAzulclaro,             5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2115,   ShortRosa,                  5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2116,   ShortPreto,                 5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2117,   ShortLaranja,               5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2118,   ShortRoxo,                  5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2119,   ShortVerdeEscuro,           5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2170,   MinissaiaVermelha,          5,      1000,       500,        8,      0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2171,   MinissaiaVerde,             5,      1000,       500,        8,      0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2172,   MinissaiaAzulEscura,        5,      1000,       500,        8,      0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2173,   MinissaiaAmarela,           5,      1000,       500,        8,      0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2174,   MinissaiaAzulClara,         5,      1000,       500,        8,      0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2175,   MinissaiaRosa,              5,      1000,       500,        8,      0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2176,   MinissaiaPreta,             5,      1000,       500,        8,      0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2177,   MinissaiaLaranja,           5,      1000,       500,        8,      0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2178,   MinissaiaRoxa,              5,      1000,       500,        8,      0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2179,   MinissaiaVerdeEscura,       5,      1000,       500,        8,      0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
2500,   CalcaAlgodaoVermelho,       5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2501,   CalcaAlgodaoVerde,          5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2502,   CalcaAlgodaoAzulEscuro,     5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2503,   CalcaAlgodaoAmarelo,        5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2504,   CalcaAlgodaoAzulClaro,      5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2505,   CalcaAlgodaoRosa,           5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2506,   CalcaAlgodaoPreto,          5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2507,   CalcaAlgodaoLaranja,        5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2508,   CalcaAlgodaoRoxo,           5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
2509,   CalcaAlgodaoVerdeEscuro,    5,      1000,       500,        20,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
3048,   Minissaia,                  5,      1000,       500,        8,      0,      2,      0,      0,      0,      0,      1,      0,      1,      0,      {},     {}
3095,   CalcaDoDragao,              5,      250000,     125000,     100,    0,      7,      0,      75,     0,      2,      1,      0,      60,     0,      {},     {bonus bMdef, 5;}
//3261, SARAB_RES_SaiaOdalisca,     5,      12000,      6000,       20,     0,      2,      0,      2,      0,      0,      1,      0,      60,     0,      {},     {} 
3267,   CalcaAlgodao,               5,      1000,       500,        15,     0,      2,      0,      0,      0,      2,      1,      0,      1,      0,      {},     {}
3327,   CalcaCavaleiroNegro,        5,      20000,      10000,      100,    0,      6,      0,      75,     0,      2,      1,      0,      50,     0,      {},     {}
3332,   CalcaCavaleiroDemon,        5,      2000,       1000,       400,    0,      6,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {}
3363,   CalcaNatalina,              5,      2000,       1000,       160,    0,      2,      0,      0,      0,      1,      1,      0,      1,      0,      {},     {}
3368,   CalcaNatalinaVerde,         5,      2000,       1000,       160,    0,      2,      0,      0,      0,      1,      1,      0,      1,      0,      {},     {}
3855,   ShortJeans1,                5,      2000,       1000,       25,     0,      5,      0,      0,      0,      2,      1,      0,      30,     0,      {},     {}
3856,   ShortJeans2,                5,      2000,       1000,       25,     0,      5,      0,      0,      0,      2,      1,      0,      30,     0,      {},     {bonus bMaxHP, 10;}
3857,   ShortJeans3,                5,      2000,       1000,       25,     0,      5,      0,      0,      0,      2,      1,      0,      30,     0,      {},     {bonus bMaxHP, 20;}
3858,   ShortJeans4,                5,      2000,       1000,       25,     0,      5,      0,      0,      0,      2,      1,      0,      30,     0,      {},     {bonus bMaxHP, 30;}
3859,   ShortJeans5,                5,      2000,       1000,       25,     0,      5,      0,      0,      0,      2,      1,      0,      30,     0,      {},     {bonus bMaxHP, 30; bonus bVit, 1;}
3860,   ShortJeans6,                5,      2000,       1000,       25,     0,      5,      0,      0,      0,      2,      1,      0,      30,     0,      {},     {bonus bMaxHP, 35; bonus bVit, 1;}
3861,   ShortJeans7,                5,      2000,       1000,       25,     0,      5,      0,      0,      0,      2,      1,      0,      30,     0,      {},     {bonus bMaxHP, 40; bonus bVit, 1;}
3862,   ShortJeans8,                5,      2000,       1000,       25,     0,      6,      0,      0,      0,      2,      1,      0,      30,     0,      {},     {bonus bMaxHP, 40; bonus bVit, 1;}
3863,   ShortJeans9,                5,      2000,       1000,       25,     0,      6,      0,      0,      0,      2,      1,      0,      30,     0,      {},     {bonus bMaxHP, 50; bonus bVit, 2;}
3864,   CalcaDeCouroDeCobra1,       5,      2000,       1000,       60,     0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {}
3865,   CalcaDeCouroDeCobra2,       5,      2000,       1000,       60,     0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 10;}
3866,   CalcaDeCouroDeCobra3,       5,      2000,       1000,       60,     0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 20;}
3867,   CalcaDeCouroDeCobra4,       5,      2000,       1000,       60,     0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 30;}
3868,   CalcaDeCouroDeCobra5,       5,      2000,       1000,       60,     0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 30; bonus bVit, 1;}
3869,   CalcaDeCouroDeCobra6,       5,      2000,       1000,       60,     0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 35; bonus bVit, 1;}
3870,   CalcaDeCouroDeCobra7,       5,      2000,       1000,       60,     0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 40; bonus bVit, 1;}
3871,   CalcaDeCouroDeCobra8,       5,      2000,       1000,       60,     0,      8,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 40; bonus bVit, 1;}
3872,   CalcaDeCouroDeCobra9,       5,      2000,       1000,       60,     0,      8,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 50; bonus bVit, 2;}
3873,   CalcaDoDragao1,             5,      250000,     125000,     100,    0,      8,      0,      75,     0,      2,      1,      0,      60,     0,      {},     {bonus bMdef, 5;}
3874,   CalcaDoDragao2,             5,      250000,     125000,     100,    0,      8,      0,      75,     0,      2,      1,      0,      60,     0,      {},     {bonus bMdef, 5; bonus bMaxHP, 10;}
3875,   CalcaDoDragao3,             5,      250000,     125000,     100,    0,      8,      0,      75,     0,      2,      1,      0,      60,     0,      {},     {bonus bMdef, 5; bonus bMaxHP, 20;}
3876,   CalcaDoDragao4,             5,      250000,     125000,     100,    0,      8,      0,      75,     0,      2,      1,      0,      60,     0,      {},     {bonus bMdef, 5; bonus bMaxHP, 30;}
3877,   CalcaDoDragao5,             5,      250000,     125000,     100,    0,      8,      0,      75,     0,      2,      1,      0,      60,     0,      {},     {bonus bMdef, 5; bonus bMaxHP, 30; bonus bVit, 1;}
3878,   CalcaDoDragao6,             5,      250000,     125000,     100,    0,      8,      0,      75,     0,      2,      1,      0,      60,     0,      {},     {bonus bMdef, 5; bonus bMaxHP, 35; bonus bVit, 1;}
3879,   CalcaDoDragao7,             5,      250000,     125000,     100,    0,      8,      0,      75,     0,      2,      1,      0,      60,     0,      {},     {bonus bMdef, 5; bonus bMaxHP, 40; bonus bVit, 1;}
3880,   CalcaDoDragao8,             5,      250000,     125000,     100,    0,      9,      0,      75,     0,      2,      1,      0,      60,     0,      {},     {bonus bMdef, 5; bonus bMaxHP, 40; bonus bVit, 1;}
3881,   CalcaDoDragao9,             5,      250000,     125000,     100,    0,      9,      0,      75,     0,      2,      1,      0,      60,     0,      {},     {bonus bMdef, 5; bonus bMaxHP, 50; bonus bVit, 2;}
3882,   CalcaCavaleiroNegro1,       5,      20000,      10000,      100,    0,      7,      0,      75,     0,      2,      1,      0,      50,     0,      {},     {}
3883,   CalcaCavaleiroNegro2,       5,      20000,      10000,      100,    0,      7,      0,      75,     0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 10;}
3884,   CalcaCavaleiroNegro3,       5,      20000,      10000,      100,    0,      7,      0,      75,     0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 20;}
3885,   CalcaCavaleiroNegro4,       5,      20000,      10000,      100,    0,      7,      0,      75,     0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 30;}
3886,   CalcaCavaleiroNegro5,       5,      20000,      10000,      100,    0,      7,      0,      75,     0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 30; bonus bVit, 1;}
3887,   CalcaCavaleiroNegro6,       5,      20000,      10000,      100,    0,      7,      0,      75,     0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 35; bonus bVit, 1;}
3888,   CalcaCavaleiroNegro7,       5,      20000,      10000,      100,    0,      7,      0,      75,     0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 40; bonus bVit, 1;}
3889,   CalcaCavaleiroNegro8,       5,      20000,      10000,      100,    0,      8,      0,      75,     0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 40; bonus bVit, 1;}
3890,   CalcaCavaleiroNegro9,       5,      20000,      10000,      100,    0,      8,      0,      75,     0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 50; bonus bVit, 2;}
3891,   CalcaCavaleiroDemon1,       5,      2000,       1000,       400,    0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {}
3892,   CalcaCavaleiroDemon2,       5,      2000,       1000,       400,    0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 10;}
3893,   CalcaCavaleiroDemon3,       5,      2000,       1000,       400,    0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 20;}
3894,   CalcaCavaleiroDemon4,       5,      2000,       1000,       400,    0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 30;}
3895,   CalcaCavaleiroDemon5,       5,      2000,       1000,       400,    0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 30; bonus bVit, 1;}
3896,   CalcaCavaleiroDemon6,       5,      2000,       1000,       400,    0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 35; bonus bVit, 1;}
3897,   CalcaCavaleiroDemon7,       5,      2000,       1000,       400,    0,      7,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 40; bonus bVit, 1;}
3898,   CalcaCavaleiroDemon8,       5,      2000,       1000,       400,    0,      8,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 40; bonus bVit, 1;}
3899,   CalcaCavaleiroDemon9,       5,      2000,       1000,       400,    0,      8,      0,      0,      0,      2,      1,      0,      50,     0,      {},     {bonus bMaxHP, 50; bonus bVit, 2;}
3900,   CalcaDeAssassino1,          5,      10000,      3000,       20,     0,      7,      0,      0,      0,      2,      1,      0,      95,     0,      {},     {bonus bAgi, 2;}
3901,   CalcaDeAssassino2,          5,      10000,      3000,       20,     0,      7,      0,      0,      0,      2,      1,      0,      95,     0,      {},     {bonus bAgi, 2; bonus bMaxHP, 10;}
3902,   CalcaDeAssassino3,          5,      10000,      3000,       20,     0,      7,      0,      0,      0,      2,      1,      0,      95,     0,      {},     {bonus bAgi, 2; bonus bMaxHP, 20;}
3903,   CalcaDeAssassino4,          5,      10000,      3000,       20,     0,      7,      0,      0,      0,      2,      1,      0,      95,     0,      {},     {bonus bAgi, 2; bonus bMaxHP, 30;}
3904,   CalcaDeAssassino5,          5,      10000,      3000,       20,     0,      7,      0,      0,      0,      2,      1,      0,      95,     0,      {},     {bonus bAgi, 2; bonus bMaxHP, 30; bonus bVit, 1;}
3905,   CalcaDeAssassino6,          5,      10000,      3000,       20,     0,      7,      0,      0,      0,      2,      1,      0,      95,     0,      {},     {bonus bAgi, 2; bonus bMaxHP, 35; bonus bVit, 1;}
3906,   CalcaDeAssassino7,          5,      10000,      3000,       20,     0,      7,      0,      0,      0,      2,      1,      0,      95,     0,      {},     {bonus bAgi, 2; bonus bMaxHP, 40; bonus bVit, 1;}
3907,   CalcaDeAssassino8,          5,      10000,      3000,       20,     0,      8,      0,      0,      0,      2,      1,      0,      95,     0,      {},     {bonus bAgi, 2; bonus bMaxHP, 40; bonus bVit, 1;}
3908,   CalcaDeAssassino9,          5,      10000,      3000,       20,     0,      8,      0,      0,      0,      2,      1,      0,      95,     0,      {},     {bonus bAgi, 2; bonus bMaxHP, 50; bonus bVit, 2;}
