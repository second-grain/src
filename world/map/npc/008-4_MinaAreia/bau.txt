008-4,82,30,0|script|[?]#008-4|332
{
    mes "[AVISO]";
    mes "Aquele que obter o poder do baú será reverenciado em todo o mundo.";
    mes "E aquele que libertar sua maldição sucumbirá o mundo inteiro em trevas.";
    close;
}

//008-4,94,38,0|script|Debug|111
//{
//    set QUEST_demon, 3;
//    set Mobpt, 50000;
//}

008-4,93,38,0|script|Baú Amaldiçoado|111
{
    if (QUEST_MASK1 & MASK1_DEMON) goto L_acabou;
    if (QUEST_demon < 1) goto L_This_shouldn_t_happen;
    if (QUEST_demon == 2) goto L_Finished;
    //if (QUEST_demon == 3) goto L_criatura; // DESATIVADO por ordem de Jesusalva. Contéudo exclusivo do TMW-BR.
    if (1+1==3) goto L_criatura;

    if (countitem("GosmaDeVerme") == 0) goto L_No_maggot_slime;
    if (countitem("CogumeloPequeno") == 0) goto L_No_mushroom;
    if (countitem("Petala") == 0) goto L_No_pink_petal;

    if (countitem("Perola") == 0) goto L_No_pearl;
    if (countitem("Espinho") == 0) goto L_No_hard_spike;
    if (countitem("TocoDeMadeira") == 0) goto L_No_raw_log;

    if (countitem("AntenaRosa") == 0) goto L_No_pink_antenna;
    if (countitem("LinguaDeCobra") == 0 || countitem("LinguaCobraDaMontanha") == 0 || countitem("LinguaCobraDagrama") == 0 || countitem("LinguaCobraDaCaverna") == 0) goto L_No_tongues;
    if (countitem("ChaveDoTesouro") == 0) goto L_No_treasure_key;

    if (countitem("PilhaDeCinzas") == 0) goto L_No_ash;
    if (countitem("ErvaMauva") == 0 || countitem("ErvaCobalto") == 0 || countitem("ErvaGamboge") == 0 || countitem("ErvaAlizarina") == 0) goto L_No_herbs;
    if (countitem("PataDeInseto") == 0) goto L_No_bug_leg;

    if (countitem("AntenaLuminosa") == 0) goto L_No_lamp;
    if (countitem("MinerioDeFerro") == 0) goto L_No_iron_ore;
    if (countitem("FerraoDeEscorpiao") == 0 || countitem("FerraoEscorpiaoVermelho") == 0 || countitem("FerraoEscorpiaoPreto") == 0) goto L_No_stingers;

    if (countitem("CasuloDeSeda") == 0) goto L_No_silk;
    if (countitem("PeloBranco") == 0) goto L_No_fur;
    if (countitem("CristalEscuro") == 0) goto L_No_dark_crystal;

    if (countitem("GarrafaDAgua") == 0) goto L_No_water;

    getinventorylist;
    if (@inventorylist_count == 100 && countitem("GosmaDeVerme") > 1 && countitem("PataDeInseto") > 1 && countitem("CasuloDeSeda") > 1
        && countitem("CogumeloPequeno") > 1 && countitem("Petala") > 1 && countitem("Perola") > 1
        && countitem("Espinho") > 1 && countitem("TocoDeMadeira") > 1 && countitem("AntenaRosa") > 1
        && countitem("CristalEscuro") > 1 && countitem("MinerioDeFerro") > 1 && countitem("FerraoDeEscorpiao") > 1
        && countitem("FerraoEscorpiaoVermelho") > 1 && countitem("FerraoEscorpiaoPreto") > 1 && countitem("LinguaDeCobra") > 1
        && countitem("LinguaCobraDaMontanha") > 1 && countitem("LinguaCobraDagrama") > 1 && countitem("LinguaCobraDaCaverna") > 1
        && countitem("ErvaMauva") > 1 && countitem("ErvaCobalto") > 1 && countitem("ErvaGamboge") > 1
        && countitem("ErvaAlizarina") > 1 && countitem("ChaveDoTesouro") > 1 && countitem("AntenaLuminosa") > 1
        && countitem("PilhaDeCinzas") > 1 && countitem("PeloBranco") > 1 && countitem("GarrafaDAgua") > 1) goto L_cheio;

    delitem "GosmaDeVerme", 1;
    delitem "PataDeInseto", 1;
    delitem "CasuloDeSeda", 1;

    delitem "CogumeloPequeno", 1;
    delitem "Petala", 1;
    delitem "Perola", 1;

    delitem "Espinho", 1;
    delitem "TocoDeMadeira", 1;
    delitem "AntenaRosa", 1;

    delitem "CristalEscuro", 1;
    delitem "MinerioDeFerro", 1;
    delitem "FerraoDeEscorpiao", 1;

    delitem "FerraoEscorpiaoVermelho", 1;
    delitem "FerraoEscorpiaoPreto", 1;
    delitem "LinguaDeCobra", 1;

    delitem "LinguaCobraDaMontanha", 1;
    delitem "LinguaCobraDagrama", 1;
    delitem "LinguaCobraDaCaverna", 1;

    delitem "ErvaMauva", 1;
    delitem "ErvaCobalto", 1;
    delitem "ErvaGamboge", 1;

    delitem "ErvaAlizarina", 1;
    delitem "ChaveDoTesouro", 1;
    delitem "AntenaLuminosa", 1;

    delitem "PeloBranco", 1;
    delitem "PilhaDeCinzas", 1;
    delitem "GarrafaDAgua", 1;

    misceffect 300;
    mes "Você conseguiu realizar o ritual e o baú se abriu.";
    getitem "MascaraDeMadeira", 1;
    mes "Dentro do baú você encontrou uma máscara de madeira.";
    set QUEST_demon, 2;
    mes "Estranhamente, você conseguiu ver algo mais dentro do baú... Dois pequenos pontos com um brilho ofuscante! Mas quando você estendeu novamente sua mão, o baú se fechou sozinho com toda a força.";
    close;

L_No_maggot_slime:
    mes "Você não tem a gosma de verme.";
    close;

L_No_mushroom:
    mes "Você não tem o cogumelo.";
    close;

L_No_pink_petal:
    mes "Você não tem a pétala cor de rosa.";
    close;

L_No_pearl:
    mes "Você não tem uma pérola do mar.";
    close;

L_No_hard_spike:
    mes "Você não tem o espinho.";
    close;

L_No_raw_log:
    mes "Você não tem nenhuma madeira.";
    close;

L_No_pink_antenna:
    mes "Você não tem a antena necessária para o ritual.";
    close;

L_No_tongues:
    mes "Você ainda não tem os quatro tipos de lingua de cobra.";
    close;

L_No_treasure_key:
    mes "Você ainda não tem uma chave.";
    close;

L_No_ash:
    mes "Você ainda não tem uma pilha de cinzas.";
    close;

L_No_herbs:
    mes "Você ainda não tem os quatro tipos de ervas.";
    close;

L_No_bug_leg:
    mes "Você ainda não tem um inseto... ou parte dele.";
    close;

L_No_lamp:
    mes "Você ainda não tem uma lâmpada natural.";
    close;

L_No_iron_ore:
    mes "Você ainda não tem o elemento natural usado para fazer armaduras.";
    close;

L_No_stingers:
    mes "Você ainda não tem os três tipos de veneno de escorpião.";
    close;

L_No_silk:
    mes "Você ainda não tem o local onde as borboletas ganham vida.";
    close;

L_No_fur:
    mes "Você ainda não entendeu o que significa \"o toque macio e delicado de uma criatura sem maldade.\" Mas você precisa disso para o ritual.";
    close;

L_No_dark_crystal:
    mes "Falta o exemplar natural da escuridão.";
    close;

L_No_water:
    mes "Você não tem a água necessária para iniciar o ritual.";
    close;

L_This_shouldn_t_happen:
    mes "Suas mãos tremem involuntariamente ao tentar tocar o baú. Tentar abri-lo agora não parece ser uma boa ideia. Surge em sua mente a ideia de aprender mais sobre este estranho baú, talvez em algum livro antigo... Mas onde?";
    close;

L_Finished:
    mes "Parece que não é uma boa ideia abrir este baú sem saber melhor o que há dentro dele.";
    close;

L_criatura:
    getinventorylist;
    if (@inventorylist_count == 100) goto L_cheio;
    if (BaseLevel < 60) goto L_fraco;
    mes "Sabendo o que existe dentro do baú, você tem a exata noção de que antes de abri-lo é preciso criar uma barreira de proteção em sua volta. Colocar o mundo em perigo não é uma opção para você!";
    next;
    menu
        "Criar a barreira.", L_Barreira,
        "Desistir.",         L_fim;

L_Barreira:
    next;
    if (Mobpt < 50000) goto L_naobarreira;
    mes "Com o sacrifício dos pontos de monstro, uma aura praticamente invisível passa a envolver o espaço em volta do baú.";
    next;
    mes "Você consegue passar pela barreira como se nada houvesse no caminho. O que fazer agora?";
    next;
    menu
        "Abrir o baú.",        L_Abrir,
        "Desistir do ritual.", L_fim;

L_Abrir:
    next;
    mes "Você abre o baú, mas ele parece estar vazio... O que fazer?";
    menu
        "Atrair a criatura para a superfície.", L_Atrair,
        "Desistir do ritual.",                  L_fim;

L_Atrair:
    next;
    if (countitem("AlmaDeJackO") < 5) goto L_naoatraiu;
    delitem "AlmaDeJackO", 5;
    mes "De repente você sente um frio congelante...Tudo em sua volta escureçe. Um som grave e constante envolve o ambiente.";
    next;
    mes "Eis que você se vê diante do incomensurável... Uma criatura indescritível.";
    next;
    mes "Você precisa agir rapidamente, pois a criatura aparenta ter um apetite especial por sua alma. O que fazer?";
    next;
    menu
        "Fugir!",                        L_fim,
        "Atacar o monstro!",             L_morre,
        "Tentar fragilizar a criatura.", L_Fragilizar;

L_Fragilizar:
    if (countitem("PocaoDeOleoDeMonstro") < 1) goto L_naofragiliza;
    delitem "PocaoDeOleoDeMonstro", 1;
    mes "Você age mais rápido que a besta e antes que ela pudesse sair por completo de dentro do baú, você derrama uma poção de óleo de monstro sobre ela.";
    next;
    mes "A poção não causou qualquer dano à criatura, mas você notou uma mudança em seu olhar... É como se a poção a tivesse colocado em um estado de confusão mental.";
    next;
    mes "Talvez seja a hora certa para aproveitar este fator e tentar um contato verbal com o monstro: ";
    input @contato$;
    next;
    if (@contato$ != "PTEROEREBUS") goto L_naocontato;
    mes "Você não consegue acreditar em seus próprios olhos!";
    next;
    mes "O monstro ficou instantaneamente paralisado ao ouvir suas palavras.";
    next;
    mes "Você é capaz de sentir o medo vindo do olhar da criatura! O que fazer agora?";
    next;
    menu
        "Atacar o monstro com o seu golpe mais forte!", L_Atacar,
        "Aplacar o sofrimento da pobre criatura.",      L_aplacar;

L_Atacar:
    if (getequipid(equip_hand1) != 3004) goto L_naoarma;
    next;
    mes "Você concentra toda sua força em seus braços e parte com toda a força para cima da criatura!";
    next;
    mes "Com o impacto do golpe, sua foice da morte fica cravada no corpo do monstro e ele acorda de seu estado de paralização!";
    next;
    mes "Imediatamente ele emite um grito terrível! Inimaginável! A expressão ensurdecedora de uma dor gigantesca!";
    next;
    mes "Você não se deixa intimidar pelo espetáculo bizarro apresentado pela criatura e mantém suas mãos firmes!";
    next;
    mes "Diante de sua insistência, a criatura fecha os olhos, se concentra por um segundo e em seguida diz uma palavra desconhecida... provavelmente a invocação de uma magia.";
    next;
    mes "Ao proferir a invocação, uma aura negra toma conta da criatura e ela desaparece por completo de seu campo de visão!";
    next;
    mes "No entanto, você percebe um objeto brilhante na ponta da lâmina de sua foice! Parece que para escapar, o monstro precisou abrir mão de uma pequena parte de seu poder, que se materializou em um anel. Sim, parece que é o anel de que falava o bilhete!";
    next;
    getitem "AnelDoDemonio", 1;
    mes "Ao olhar para o baú, você percebe que ele se fechou sozinho, aprisionando a terrível criatura para sempre...";
    set QUEST_MASK1, (QUEST_MASK1 | MASK1_DEMON);
    set QUEST_demon, 0;
    close;

L_naobarreira:
    mes "Você não tem a quantidade de pontos de monstro para criar a barreira. Ainda faltam pelo menos " + (50000 - Mobpt) + " pontos de monstro.";
    close;

L_naoatraiu:
    mes "Você ainda não sabe como atrair a criatura.";
    close;

L_naofragiliza:
    mes "Você ainda não conseguiu fragilizar a criatura.";
    close;

L_naocontato:
    mes "Suas palavras provocaram a ira da terrível criatura!";
    next;
    mes "Sem ao menos tocar em você, o monstro te lança para longe, acabando com toda sua vitalidade";
    close2;
    warp "008-4", 89, 71;
    heal -100, 0;
    end;

L_aplacar:
    mes "Sem medo, você se aproxima ainda mais do monstro...";
    next;
    mes "Com um olhar cândido e palavras doces, você tenta ganhar a confiança do monstro.";
    next;
    mes "Mas tudo isso em vão! Pois a criatura irrompe em um ataque de fúria e te ataca brutalmente!";
    next;
    close2;
    heal -20, 0;
    warp "008", 171, 101;
    message strcharinfo(0), "Com uma forte dor de cabeça, você mal consegue lembrar o que aconteceu.";
    end;

L_naoarma:
    mes "Você concentra toda sua força em seus braços e parte com toda a força para cima da criatura!";
    next;
    mes "Mas seu golpe não passa de um mero toque na resistente pela do monstro!";
    next;
    mes "Você tem sorte que o monstro está paralisado, pois do contrário a reação seria fatal.";
    close;

L_cheio:
    mes "Você não tem espaço no seu inventário. Não é prudente começar este ritual sem espaço para novos itens.";
    close;

L_morre:
    mes "Antes mesmo de você assumir a posição de ataque, a criatura retira toda sua vitalidade com uma espécie de magia obscura.";
    heal -(MaxHp), 0;
    close;

L_fim:
    close;

L_fraco:
    set @lvl, (60 - BaseLevel);
    if (@lvl == 1) set @nivel$, "nível";
    if (@lvl > 1) set @nivel$, "níveis";
    mes "Pensando melhor, você decide não se meter com uma criatura desse porte enquanto não ganhar pelo menos mais " + @lvl + " " + @nivel$ + ".";
    close;

L_acabou:
    mes "Suas mãos se aproximam do baú, prontas para abrí-lo, mas sua mente te impede de executar esta ação...";
    close;
}
