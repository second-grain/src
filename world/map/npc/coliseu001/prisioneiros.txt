// AVISO: O Cabeçalho de Aviso de Quest deve ficar sempre antes do Cabeçalho de Dialogo


//------Cabeçalho de Aviso de Quest -------------------------------------------

coliseu001,142,205,0|script|Prisioneiro|300,2,15
{
    if (QUEST_Prisioneiro > 0) end;
    if (BaseLevel < 40) goto L_semLVL;
    emotion EMOTE_QUEST;
    end;

L_semLVL:
    set @n, rand(8);
    if (@n == 0 && BaseLevel < 40 ) npctalk strnpcinfo(0), "Socorro! Socorro!!";
    if (@n == 1 && BaseLevel < 40 ) npctalk strnpcinfo(0), "Oh! E agora quem poderá me defender?";
    if (@n == 2 && BaseLevel < 40 ) npctalk strnpcinfo(0), "Eu sou inocente, eu juro!";
    if (@n == 3 && BaseLevel < 40 ) npctalk strnpcinfo(0), "Mãe do Céu, não quero morrer aqui!";
    if (@n == 4 && BaseLevel < 40 ) npctalk strnpcinfo(0), "Por tudo quanto é mais sagrado me liberte!";
    if (@n == 5 && BaseLevel < 40 ) npctalk strnpcinfo(0), "Eu juro que serei bonzinho!";
    if (@n == 6 && BaseLevel < 40 ) npctalk strnpcinfo(0), "Não quero morrer aqui dentro!";
    if (@n == 7 && BaseLevel < 40 ) npctalk strnpcinfo(0), "Não quero morrer me degladiando no coliseu!";
    end;
}


//------Cabeçalho de Dialogo -------------------------------------------
coliseu001,142,205,0|script|#Prisioneiro01|313,1,1
{
    if (BaseLevel < 40) goto L_semLVL;
    mes "[Prisioneiro]";
    mes "\"Me tire daquiiii! Eu sou inoceeeente.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Qual foi o seu crime?\"";
    //next;
    //mes "início da quest...";
    close;
    
L_semLVL:
    mes "[Prisioneiro]";
    mes "\"Será possivel que não tem ninguem aqui forte o suficiente para me ajudar? Cho! Cho! Cho! Sai! Vaza!\"";
    close;
}
