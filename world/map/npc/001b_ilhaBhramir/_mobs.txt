001b,66,42,39,34|monster|Rama de Batata|1341,10,0ms,0ms,Mob001b::On1341
001b,51,103,65,34|monster|Cogumelo|1019,10,0ms,0ms,Mob001b::On1019
001b,57,78,52,34|monster|Trasgo Rosa|1018,10,0ms,0ms,Mob001b::On1018
001b,80,33,20,14|monster|Tartaruga|1340,4,0ms,300000ms,Mob001b::On1340


001b,0,0,0|script|Mob001b|32767
{
    end;

On1018:
    set @mobID, 1018;
    callfunc "MobPoints";
    callsub S_MOBS_trasgo;
    end;

On1019:
    set @mobID, 1019;
    callfunc "MobPoints";
    end;

On1340:
    set @mobID, 1340;
    callfunc "MobPoints";
    callsub S_MOBS_queimaduraTartaruga;
    end;

On1341:
    set @mobID, 1341;
    callfunc "MobPoints";
    end;


//= Callsubs ==========================================================

S_MOBS_queimaduraTartaruga:
    if (QUEST_pocaoQueimadura != 2) goto L_Return;
    set @max, 20;
    set @mobs, MOBS_queimaduraTartaruga;
    set @flag, @MOBS_queimaduraTartaruga;
    callfunc "mobContagem";
    set MOBS_queimaduraTartaruga, @mobs;
    set @MOBS_queimaduraTartaruga, @flag;
    return;

S_MOBS_trasgo:
    if (QUEST_trasgo != 8) goto L_Return;
    set @max, 35;
    set @mobs, MOBS_trasgo;
    set @flag, @MOBS_trasgo;
    callfunc "mobContagem";
    set MOBS_trasgo, @mobs;
    set @MOBS_trasgo, @flag;
    return;

L_Return:
    return;
}
