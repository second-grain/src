hali001,138,63,0|script|Fred Treinador|102,10,10
{
    if ($PosseDeHalicarnazo != 0 && Reino != $PosseDeHalicarnazo) goto L_SugestaoAoInvasor;
    set @npcname$, "Fred Treinador";
    callfunc "treinador";
    close;

L_SugestaoAoInvasor:
    heal -(MaxHp / 10), 0;
    set @n, rand(4);
    if (@n == 0) npctalk strnpcinfo(0), "Saia daqui seu invasor de Halicarnazo! (Chute!)";
    if (@n == 1) npctalk strnpcinfo(0), "Você não pode entrar em Halicarnazo! (Pontapé!)";
    if (@n == 2) npctalk strnpcinfo(0), "Como você se atreve a tentar machucar as pessoas do Reino de Halicarnazo? (Soco!)";
    if (@n == 3) npctalk strnpcinfo(0), "Vou lhe dar uma bengalada! (Soco!)";
    end;

OnTouch:
    if ($PosseDeHalicarnazo != 0 && Reino != $PosseDeHalicarnazo) goto L_Raiva;
    if (BaseLevel >= 20) end;
    emotion EMOTE_QUEST;
    end;

L_Raiva:
    emotion EMOTE_UPSET;
    heal -(MaxHp / 10), 0;
    end;
}
