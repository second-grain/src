navio010,60,23,0|script|Baú#navio010|111
{
    set @npc$, "Baú";
    set @map$, "navio010";
    set @x, 60;
    set @y, 24;
    callfunc "objectSaver";
    close;
}

navio010,60,24,0|script|#Pena-navio010|32767,1,1
{
    callfunc "limparDist";
    end;
}

navio010,60,24,0|script|#Zona-navio010|32767,1,1
{
    if (ZONA_SEGURA == 1) message strcharinfo(0), "Estou fora da zona segura.";
    set ZONA_SEGURA, 0; // Na saída do PVP a zona deixa de ser segura.
    close;
}
