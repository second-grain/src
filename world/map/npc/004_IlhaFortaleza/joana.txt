// @autor Diogo_RBG - http://diogorbg.blogspot.com
// @desc Joana adora as tortas de maçã de sua prima Cornélia.
// #quest <desc>, <getItens>, <delItens>, <extra>
// @quest 'Cesta de Maçãs', '1 cestaMacas', '', 'Esperar 1:00'
// @quest '+ Gratidão', '10 Apple', '', ''


//004,48,101,0|script|time|163
//{
//    set QUEST_praiaTime, QUEST_praiaTime - 60;
//    mes "time avançou 60 seg.";
//    close;
//}


// Proposta de modificações em variáveis:
// QUEST_TortaMaca == QUEST_praia - Economia de variáveis nos NPCs joana e cornelia e pablo

004,51,101,0|script|Joana|163
{
    if (QUEST_praia == 2 && gettimetick(2) >= QUEST_praiaTime + 60) goto L_pronto;
    if (QUEST_praia == 2) goto L_espere;
    if (QUEST_praia == 3) goto L_entregue;
    if (QUEST_praia == 4) goto L_tortaAssando;
    if (QUEST_praia == 5) goto L_tortaPronta;
    if (QUEST_praia >= 6) goto L_ok;

    mes "[Joana]";
    mes "\"Olá " + @my$ + " jovem. Eu estou colhendo maçãs para que minha prima faça uma torta. Ela faz tortas deleciosas, sabia?\"";
    if (QUEST_praia == 0) goto L_fala1;
    if (QUEST_praia == 1) goto L_fala2;
    close;

L_fala1:
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Quem é sua prima?", L_quem,
        "Hum... até mais.",  L_Fechar;

L_quem:
    mes "";
    mes "[Joana]";
    mes "\"Minha prima se chama Cornélia e mora em uma cabana na praia do porto.\"";
    next;
    mes "[" + strcharinfo (0) + "]";
    mes "\"Obrigad" + @fm$ + " pela informação. Talvez eu compre tortas dela.\"";
    close;

L_fala2:
    next;
    mes "[" + strcharinfo (0) + "]";
    mes "\"Acabei de falar com sua prima e ela está preocupada com sua demora.\"";
    next;
    mes "[Joana]";
    mes "\"É mesmo. Já faz muito tempo que saí para colher maças. Mas já estou quase acabando.";
    mes "Eu ficaria muito grata se você pudesse avisar a ela, ou pudesse me esperar... só vou levar 1 minuto.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Ok... vou avisar a ela.", L_OK,
        "Ok... vou esperar.",      L_OK;

L_OK:
    set QUEST_praiaTime, gettimetick(2);
    set QUEST_praia, 2;
    close;

L_espere:
    mes "[Joana]";
    mes "\"Já estou quase acabando de colher as maçãs.";
    mes "Eu ficaria muito grata se você pudesse avisar a ela, ou pudesse me esperar... só vou levar 1 minuto.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Ok... vou avisar a ela.", L_Fechar,
        "Ok... vou esperar.",      L_Fechar;

L_pronto:
    set QUEST_praiaTime, 0;
    mes "[Joana]";
    mes "\"Ponto... acabei de colher as maçãs. Ufa!";
    mes "Por gentileza, você poderia me fazer um favor?\"";
    next;
    mes "[Joana]";
    mes "\"Você poderia levar esta cesta de maçãs para minha prima Cornélia?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Você prometeu as maçãs... você leva!", L_Fechar,
        "Farei agora mesmo.",                   L_levarCesta;

L_levarCesta:
    mes "";
    mes "[Joana]";
    mes "\"Você é realmente muito gentil!\"";
    next;
    getinventorylist;
    if ((@inventorylist_count + (countitem("CestaMacas") == 0)) > 100) goto L_cheio;

    mes "[Joana]";
    mes "\"Leve esta cesta para minha prima Cornélia e diga a ela que amanhã irei provar de sua deliciosa torta.\"";
    getitem "CestaMacas", 1;
    set QUEST_praia, 3;
    close;

L_entregue:
    mes "[Joana]";
    mes "\"Por favor, entregue a cesta de maçãs que te dei para minha prima Cornélia.\"";
    close;

//=====================================================================

L_tortaAssando:
    mes "[Joana]";
    mes "\"Entregou a cesta de maças para minha prima?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Sim... inclusive ela já está preparando a torta.\"";
    close;

L_tortaPronta:
    mes "[Joana]";
    mes "\"Entregou a cesta de maças para minha prima?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Claro!",                                     L_Claro,
        "Sim. E tenho uma fatia de torta para você.", L_darTorta;

L_Claro:
    getinventorylist;
    if ((@inventorylist_count + (countitem("Maca") == 0)) > 100) goto L_cheio;

    mes "";
    mes "[Joana]";
    mes "\"Obrigada!";
    mes "Fique com estas maçãs... Um pequeno pagamento por ter me ajudado.\"";
    getitem "Maca", 10;
    set QUEST_praia, 6;
    close;

L_darTorta:
    if (countitem("TortaDeMaca") < 1) goto L_semTorta;
    getinventorylist;
    if ((@inventorylist_count + (countitem("Maca") == 0)) > 100) goto L_cheio;

    mes "";
    mes "[Joana]";
    mes "\"Eu adoro esta torta de maçã que minha prima faz. É deliciosa!";
    mes "Fique com estas maçãs... Um pequeno pagamento por ter me ajudado.\"";
    delitem "TortaDeMaca", 1;
    getitem "Maca", 30;
    set QUEST_praia, 6;
    close;

L_semTorta:
    mes "";
    mes "[Joana]";
    mes "\"Onde está a torta? Não estou vendo!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Me desculpe... me enganei!\"";
    close;

L_cheio:
    mes "";
    mes "[Joana]";
    mes "\"Pretendia lhe dar algo, mas parece que sua sacola está cheia.";
    mes "Volte mais tarde.\"";
    close;

L_ok:
    mes "[Joana]";
    mes "\"Obrigada por me ajudar!\"";
    close;

L_Fechar:
    close;
}
