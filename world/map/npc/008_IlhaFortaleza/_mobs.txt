008,162,103,28,12|monster|Escorpião|1003,5,30000ms,15000ms,Mob008::On1003
008,163,102,28,12|monster|Verme|1002,10,0ms,0ms,Mob008::On1002
008,137,90,39,15|monster|Trasgo de Fogo|1011,5,0ms,0ms,Mob008::On1011
008,102,52,160,64|monster|Bicho da Seda|1035,40,0ms,0ms,Mob008::On1035
008,149,69,59,26|monster|Tronco Vivo|1025,8,0ms,0ms,Mob008::On1025
008,170,68,13,18|monster|Planta Alizarina|1032,1,240000ms,120000ms,Mob008::On1032
008,171,67,13,18|monster|Planta Gamboge|1031,1,240000ms,120000ms,Mob008::On1031
008,157,45,43,18|monster|Tronco Vivo|1025,5,0ms,0ms,Mob008::On1025
008,171,46,13,18|monster|Planta Cobalto|1030,1,240000ms,120000ms,Mob008::On1030
008,172,45,13,18|monster|Planta Mauva|1029,1,240000ms,120000ms,Mob008::On1029
008,134,42,56,13|monster|Bicho da Seda|1035,5,0ms,0ms,Mob008::On1035
008,116,68,34,25|monster|Flor|1014,5,0ms,0ms,Mob008::On1014
008,46,64,40,17|monster|Trasgo de Fogo|1011,5,0ms,0ms,Mob008::On1011
008,84,93,22,20|monster|Flor|1014,3,0ms,0ms,Mob008::On1014
008,60,82,23,12|monster|Morcego|1017,5,120000ms,0ms,Mob008::On1017
008,50,94,12,7|monster|Verme|1002,5,0ms,0ms,Mob008::On1002
008,72,41,23,12|monster|Morcego|1017,5,120000ms,0ms,Mob008::On1017
008,69,45,47,18|monster|Flor|1014,5,0ms,0ms,Mob008::On1014
008,158,97,1,4|monster|Escorpião Vermelho|1004,1,90000ms,30000ms,Mob008::On1004
008,37,46,0,0|monster|Colméia|1337,1,60000ms,0ms,Mob008::On1337_1
008,64,25,0,0|monster|Colméia|1337,1,60000ms,0ms,Mob008::On1337_2
008,148,29,0,0|monster|Colméia|1337,1,60000ms,0ms,Mob008::On1337_3
008,41,31,0,0|monster|Colméia|1337,1,60000ms,0ms,Mob008::On1337_4
008,28,69,0,0|monster|Colméia|1337,1,60000ms,0ms,Mob008::On1337_5


008,0,0,0|script|Mob008|32767
{
    end;

On1002:
    set @mobID, 1002;
    callfunc "MobPoints";
    callsub L_MOBS_verme;
    end;

On1003:
    set @mobID, 1003;
    callfunc "MobPoints";
    callsub L_MOBS_escorpiao;
    callsub L_MOBS_queimaduraEscorpiao;
    end;

On1004:
    set @mobID, 1004;
    callfunc "MobPoints";
    end;

On1011:
    set @mobID, 1011;
    callfunc "MobPoints";
    end;

On1014:
    set @mobID, 1014;
    callfunc "MobPoints";
    end;

On1017:
    set @mobID, 1017;
    callfunc "MobPoints";
    end;

On1025:
    set @mobID, 1025;
    callfunc "MobPoints";
    callsub L_MOBS_cordaPescador;
    callsub L_MOBS_madeiraResist;
    callsub L_MOBS_escudoMadeira;
    callsub L_MOBS_pedraAguia;
    callfunc "drop_ambar_pedraAguia";
    end;

On1029:
    set @mobID, 1029;
    callfunc "MobPoints";
    end;

On1030:
    set @mobID, 1030;
    callfunc "MobPoints";
    end;

On1031:
    set @mobID, 1031;
    callfunc "MobPoints";
    end;

On1032:
    set @mobID, 1032;
    callfunc "MobPoints";
    end;

On1035:
    set @mobID, 1035;
    callfunc "MobPoints";
    end;

On1337_1:
    set @mobID, 1337;
    callfunc "MobPoints";
    set @mapa$, "008";
    set @x, 37;
    set @y, 46;
    set @label$, "Mob008::On1049_1";
    callfunc "sumonaAbelhas";
    end;

On1049_1:
    set @mobID, 1049;
    callfunc "MobPoints";
    callsub L_MOBS_abelha;
    end;

On1337_2:
    set @mobID, 1337;
    callfunc "MobPoints";
    set @mapa$, "008";
    set @x, 64;
    set @y, 25;
    set @label$,"Mob008::On1049_2";
    callfunc "sumonaAbelhas";
    end;
On1049_2:
    set @mobID, 1049;
    callfunc "MobPoints";
    callsub L_MOBS_abelha;
    end;

On1337_3:
    set @mobID, 1337;
    callfunc "MobPoints";
    set @mapa$, "008";
    set @x, 148;
    set @y, 29;
    set @label$, "Mob008::On1049_3";
    callfunc "sumonaAbelhas";
    end;

On1049_3:
    set @mobID, 1049;
    callfunc "MobPoints";
    callsub L_MOBS_abelha;
    end;

On1337_4:
    set @mobID, 1337;
    callfunc "MobPoints";
    set @mapa$, "008";
    set @x, 41;
    set @y, 31;
    set @label$, "Mob008::On1049_4";
    callfunc "sumonaAbelhas";
    end;

On1049_4:
    set @mobID, 1049;
    callfunc "MobPoints";
    callsub L_MOBS_abelha;
    end;

On1337_5:
    set @mobID, 1337;
    callfunc "MobPoints";
    set @mapa$, "008";
    set @x, 28;
    set @y, 69;
    set @label$, "Mob008::On1049_5";
    callfunc "sumonaAbelhas";
    end;
On1049_5:
    set @mobID, 1049;
    callfunc "MobPoints";
    callsub L_MOBS_abelha;
    end;

//= Callsubs ==========================================================

L_MOBS_abelha:
    if (QUEST_ursinho != 1 && QUEST_ursinho != 2) goto L_Return;
    set @max, 1125;
    set @mobs, QUEST_ursinhoAbelhas;
    set @flag, @QUEST_ursinhoAbelhas;
    callfunc "mobContagem";
    set QUEST_ursinhoAbelhas, @mobs;
    set @QUEST_ursinhoAbelhas, @flag;
    return;

L_MOBS_cordaPescador:
    if (QUEST_cordaPescador != 1) goto L_Return;
    set @max, 30;
    set @mobs, MOBS_cordaPescador;
    set @flag, @MOBS_cordaPescador;
    callfunc "mobContagem";
    set MOBS_cordaPescador, @mobs;
    set @MOBS_cordaPescador, @flag;
    return;

L_MOBS_escorpiao:
    if (QUEST_praia != 12) goto L_Return;
    set @max, 30;
    set @mobs, QUEST_contamobs;
    set @flag, @QUEST_contamobs;
    callfunc "mobContagem";
    set QUEST_contamobs, @mobs;
    set @QUEST_contamobs, @flag;
    return;

L_MOBS_escudoMadeira:
    if (QUEST_escudoMadeira != 1) goto L_Return;
    set @max, 130;
    set @mobs, MOBS_escudoMadeira;
    set @flag, @MOBS_escudoMadeira;
    callfunc "mobContagem";
    set MOBS_escudoMadeira, @mobs;
    set @MOBS_escudoMadeira, @flag;
    return;

L_MOBS_madeiraResist:
    if (QUEST_madeiraResist != 1) goto L_Return;
    set @max, 100;
    set @mobs, MOBS_madeiraResist;
    set @flag, @MOBS_madeiraResist;
    callfunc "mobContagem";
    set MOBS_madeiraResist, @mobs;
    set @MOBS_madeiraResist, @flag;
    return;

L_MOBS_pedraAguia:
    if (QUEST_pedraAguia != 1) goto L_Return;
    set @max, 200;
    set @mobs, MOBS_pedraAguia;
    set @flag, @MOBS_pedraAguia;
    callfunc "mobContagem";
    set MOBS_pedraAguia, @mobs;
    set @MOBS_pedraAguia, @flag;
    return;

L_MOBS_queimaduraEscorpiao:
    if (QUEST_pocaoQueimadura != 2) goto L_Return;
    set @max, 20;
    set @mobs, MOBS_queimaduraEscorpiao;
    set @flag, @MOBS_queimaduraEscorpiao;
    callfunc "mobContagem";
    set MOBS_queimaduraEscorpiao, @mobs;
    set @MOBS_queimaduraEscorpiao, @flag;
    return;

L_MOBS_verme:
    if (QUEST_praia != 7) goto L_Return;
    set @max, 15;
    set @mobs, QUEST_contamobs;
    set @flag, @QUEST_contamobs;
    callfunc "mobContagem";
    set QUEST_contamobs, @mobs;
    set @QUEST_contamobs, @flag;
    return;

L_Return:
    return;
}
