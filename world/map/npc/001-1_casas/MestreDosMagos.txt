// quest MESTRE MAGO por Sky

001-1,101,58,0|script|Mari|325
{
    set @AntenaLuminosa, 612;
    set @AntenaRosa, 614;
    set @GrimoriumMago, 3208;
    set @ClasseCavaleiro, 400;
    set @ClasseDruida, 401;
    set @ClasseMago, 402;
    set @ClasseNinja, 403;
    set @ClassePaladino, 404;
    set @ClasseNecromante, 405;

    if (getskilllv(@ClasseMago) == 0 && (QUEST_Classe < 2 || BaseLevel < 40)) goto L_SemNivel;
    if (getskilllv(@ClasseMago) == 0 && QUEST_Classe == 3) goto L_Retorno;
    if (getskilllv(@ClasseMago) == 0 && QUEST_Classe == 4) goto L_JaDoutraClasse;
    if (getskilllv(@ClasseMago) >= 1) goto L_JaDestaClasse;
    goto L_iniciaQuest;

L_JaDoutraClasse:
    mes "[Mari]";
    mes "\"Agora você já pertence a classe...\"";

    set @Classe, @ClasseCavaleiro;
    callfunc "GerarNomeDaPatente"; 
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    set @Classe, @ClasseDruida;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    set @Classe, @ClasseMago;
    callfunc "GerarNomeDaPatente"; 
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    set @Classe, @ClasseNinja;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    set @Classe, @ClassePaladino;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    set @Classe, @ClasseNecromante;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    close;

L_JaDestaClasse:
    set @Classe, @ClasseMago;
    callfunc "GerarNomeDaPatente";
    mes "[Mari]";
    mes "\"Meus parabéns! Agora você é um" + @a$ + " " + @NomeDaPatente$ + "!\"";
    close;

L_SemNivel:
    mes "[Mari]";
    mes "\"Olá viajante!\"";
    close;

L_iniciaQuest:
    mes "Você vê um moça de túnica vermelha e chapéu vermelho, parace ser bem simpática.";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "(^_^) \"A senhorita é a Mestra dos Magos e das Feiticeiras?\"";
    next;
    mes "[Mari]";
    mes "(^_^) \"Sou sim! Meu nome é Mari, a Mestra dos Magos e das Feiticeiras do Mundo de Mana. Como vai?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    if (Sex == 0) mes "(^_^) \"Estou ótima! A Mestra Mari poderia me ensinar a ser uma Feiticeira?\"";
    if (Sex == 1) mes "(^_^) \"Estou ótimo! A Mestra Mari poderia me ensinar a ser um Mago?\"";
    next;
    mes "[Mari]";
    mes "(^_^) \"Primeiro, preciso saber se você será um bom aprendiz.\"";
    next;
    mes "[Mari]";
    mes "(^_^) \"Para saber isso, preciso que você me traga alguns itens.\"";
    next;
    mes "[Mari]";
    mes "(¬.¬) \"Eles são: 50 Antenas Luminosas e 30 Antenas Rosas.\"";
    next;
    mes "[Mari]";
    mes "(T_T) \"Mas antes saiba que se você escolher ser da classe dos Magos e das Feiticeiras não poderá aprender outra classe depois. Também é preciso saber se você é realmente excepcional, e se sabe o que está fazendo.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "(^-^) Como a senhorita quiser! Eu já trago o que me foi ordenado.", L_Trago,
        "(T.T) Ahh, esquece! Desisto desta porcaria!", L_Fechar;

L_Trago:
    set QUEST_Classe, 3;
    mes "";
    mes "[Mari]";
    mes "(¬.¬) \"Boa sorte!\"";
    close;

L_Retorno:
    mes "[Mari]";
    mes "\"(^-^) Trouxe os itens que pedi?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "(^-^) Sim, aqui estão.", L_Pronto,
        "(X.x) Não, Esqueci quais são!", L_Esqueci;

L_Pronto:
    if (countitem(@AntenaLuminosa) < 50 || countitem(@AntenaRosa) < 30) goto L_Esqueci;
    getinventorylist;
    if (@inventorylist_count >= 100) goto L_semlugar;
    delitem @AntenaLuminosa, 50;
    delitem @AntenaRosa, 30;
    getitem @GrimoriumMago, 1;
    setskill @ClasseMago, 1;
    set QUEST_Classe, 4;
    mes "";
    mes "[Mari]";
    mes "(^_^) \"Parabéns aprendiz! Você provou ser capaz de se esforçar. Agora você é um aprendiz dos Magos e das Feiticeiras.\"";
    next;
    mes "[Mari]";
    mes "(^_^) \"Eu te dou de presente esse Grimorium Púrpura que é o livro para você anotar os segredos guia dos Magos e Feiticeiras.\"";
    close;

L_Esqueci:
    mes "";
    mes "[Mari]";
    mes "(¬_¬) \"Bem, você não trouxe o que eu te pedi.\"";
    mes "";
    mes "[" + strcharinfo(0) + "]";
    mes "(X.x) \"Opa! Acho que perdi alguns itens por aííí.\"";
    next;
    mes "[Mari]";
    mes "(T_T) \"Não esqueça que os itens são esses!";
    mes " * 50 Antenas Luminosa.";
    mes " * 30 Antenas Rosa.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "(X.x) \"Senhora! Sim senhora! Não esquecerei senhora! Já volto senhora!\"";
    close;

L_semlugar:
    mes "[Mari]";
    mes "\"Sua bolsa está bem cheia, não é? Libere espaço e depois volte para pegar seu presente.\"";
    close;

L_Fechar:
    close;
}
