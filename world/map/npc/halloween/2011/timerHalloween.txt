// @author  Diogo_RBG - http://diogorbg.blogspot.com
// @desc    Timer do Halloween 2011


013,100,145,0|script|#timerHLW|32767
{
    mes "...";
    close;

OnMinute00:
    initnpctimer;
    callfunc "getEvento2";
    if ($@evento == 0) end;
    // Começou
    announce "* O evento 'Top15 de Halloween' está ATIVO.", 0;
    goto L_inicioEvento;

OnTimer300000:
    setnpctimer 0;
    callfunc "getEvento2";
    goto L_infoPlacar;

L_infoPlacar:
    if ($@evento == 0) end;

    announce "1-5: "+
        $HLW3_nomPlacar$[0]+" (" + $HLW3_vetPlacar[0] + "), "+
        $HLW3_nomPlacar$[1]+" (" + $HLW3_vetPlacar[1] + "), "+
        $HLW3_nomPlacar$[2]+" (" + $HLW3_vetPlacar[2] + "), "+
        $HLW3_nomPlacar$[3]+" (" + $HLW3_vetPlacar[3] + "), "+
        $HLW3_nomPlacar$[4]+" (" + $HLW3_vetPlacar[4] + ")", 0;
    announce "6-10: "+
        $HLW3_nomPlacar$[5]+" (" + $HLW3_vetPlacar[5] + "), "+
        $HLW3_nomPlacar$[6]+" (" + $HLW3_vetPlacar[6] + "), "+
        $HLW3_nomPlacar$[7]+" (" + $HLW3_vetPlacar[7] + "), "+
        $HLW3_nomPlacar$[8]+" (" + $HLW3_vetPlacar[8] + "), "+
        $HLW3_nomPlacar$[9]+" (" + $HLW3_vetPlacar[9] + ")", 0;
    announce "11-15: "+
        $HLW3_nomPlacar$[10]+" (" + $HLW3_vetPlacar[10] + "), "+
        $HLW3_nomPlacar$[11]+" (" + $HLW3_vetPlacar[11] + "), "+
        $HLW3_nomPlacar$[12]+" (" + $HLW3_vetPlacar[12] + "), "+
        $HLW3_nomPlacar$[13]+" (" + $HLW3_vetPlacar[13] + "), "+
        $HLW3_nomPlacar$[14]+" (" + $HLW3_vetPlacar[14] + ")", 0;
    end;

L_inicioEvento:
    killmonsterall "013";
    areamonster "013", 0, 0, 215, 155, "", 1364, 300, "#timerHLW::onNorte";
    areamonster "013", 60, 135, 295, 295, "", 1364, 300, "#timerHLW::onSul";
    end;

onNorte:
    callfunc "getEvento";
    callfunc "dropSemente";
    if (@evento == 0) end;
    areamonster "013", 0, 0, 215, 155, "", 1364, 1, "#timerHLW::onNorte";
    set @n, rand(100);
    if (@n < 10) getitem 1996 + rand(4), 1;
    end;

onSul:
    callfunc "getEvento";
    callfunc "dropSemente";
    if (@evento == 0) end;
    areamonster "013", 60, 135, 295, 295, "", 1364, 1, "#timerHLW::onSul";
    set @n, rand(100);
    if (@n < 10) getitem 1996 + rand(4), 1;
    end;
}
