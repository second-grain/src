froz001,106,83,0|script|Alicia|103,7,7
{
    if ($PosseDeFroz != 0 && Reino != $PosseDeFroz) goto L_SugestaoAoInvasor;
    //if (ResetA_charstate == 1) goto L_Multiple;
    //set @cost, 10000000 / ($ResetA_uses + 1);

    set @cost, BaseLevel * 100;

    mes "[Alicia]";
    mes "\"Eu tive a oportunidade de conhecer uma magia que irá resetar seus pontos de status.";
    mes "Normalmente essa magia é muito cara, mas devido à fatores sobrenaturais, conseguirei evocar a magia sem muito custo!";
    mes "Para você sairá por apenas " + @cost + " GP.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Resete meu Status", L_Reseta,
        "Esqueça, eu não quero", L_Pass;

L_Reseta:
    //set $ResetA_uses, $ResetA_uses + 1;
    //set ResetA_charstate, 1;
    if (Zeny < @cost) goto L_NoMoney;
    set Zeny, Zeny - @cost;
    resetstatus;
    mes "[Alicia]";
    mes "\"Aí está.";
    mes "";
    mes "Parece renovado!\"";
    close;

L_SugestaoAoInvasor:
    heal -(MaxHp / 4), 0;
    set @n, rand(4);
    if (@n == 0) npctalk strnpcinfo(0), "Saia daqui seu invasor de Froz! (Chute!)";
    if (@n == 1) npctalk strnpcinfo(0), "Você não pode entrar em Froz! (Pontapé!)";
    if (@n == 2) npctalk strnpcinfo(0), "Como você se atreve a tentar machucar as pessoas do Reino de Froz? (Soco!)";
    if (@n == 3) npctalk strnpcinfo(0), "Eu não presto meus serviços para usurpadores! (Golpe!)";
    end;

L_Pass:
    mes "[Alicia]";
    mes "\"Muito bem então, até mais.\"";
    close;

L_NoMoney:
    mes "[Alicia]";
    mes "\"Me desculpe mas o preço é esse e não aceito barganha.";
    mes "";
    mes "Quem sabe você não pede um empréstimo para um amigo?\"";
    close;

OnTouch:
    if ($PosseDeFroz != 0 && Reino != $PosseDeFroz) goto L_Raiva;
    emotion EMOTE_HAPPY;
    end;

L_Raiva:
    emotion EMOTE_UPSET;
    heal -(MaxHp / 4), 0;
    end;
}
