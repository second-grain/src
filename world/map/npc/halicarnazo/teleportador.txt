// Descrição: Teleporte... Apenas para voltar.


halicarnazo,78,30,0|script|Teleportadora#hali|330
{
    if (BaseLevel < 10) goto L_novoJogador;
    if (@timer_running) goto L_fechar;
    if (@intro) goto L_menu;

    set @intro, 1;
    mes "[Teleportadora Bellatrix]";
    mes "\"A ligação entre o nosso mundo e as estrelas é bem maior do que podemos imaginar.";
    mes "Eu acredito que os pontos deste mundo se ligam da mesma forma em que as estrelas se ligam no céu.\"";
    next;
    goto L_menu;

L_menu:
    mes "[Teleportadora Bellatrix]";
    mes "\"Guiada pelas estrelas eu posso te levar a muitos lugares... basta existir uma conexão entre eles.";
    mes "Para onde gostaria de ir?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Vila dos Pescadores (5.000 GP).", L_vilaPescadores,
        "Porto de Bhramir (7.000 GP).",    L_portoBhramir,
        "Centro de Bhramir (10.000 GP).",  L_centroBhramir,
        "Centro de Froz (8.000 GP).",      L_centroFroz,
        "Vila do Sarab (20.000 GP).",      L_vilaSarab,
        "Para Locais de Batalha.",         L_menuBatalha,
        "Desisti de viajar.",              L_fechar;

L_vilaPescadores:
    if (Zeny < 5000) goto L_semGrana;
    set Zeny, Zeny - 5000;

    set @map$, "001";
    set @x, 80;
    set @y, 78;
    goto L_teleportar;

L_portoBhramir:
    if (Zeny < 7000) goto L_semGrana;
    set Zeny, Zeny - 7000;

    set @map$, "007";
    set @x, 100;
    set @y, 86;
    goto L_teleportar;

L_centroBhramir:
    if (Zeny < 10000) goto L_semGrana;
    set Zeny, Zeny - 10000;

    set @map$, "005";
    set @x, 127;
    set @y, 84;
    goto L_teleportar;

L_centroFroz:
    if (Zeny < 8000) goto L_semGrana;
    set Zeny, Zeny - 8000;

    set @map$, "froz";
    set @x, 93;
    set @y, 46;
    goto L_teleportar;

L_vilaSarab:
    if (Zeny < 20000) goto L_semGrana;
    set Zeny, Zeny - 20000;

    set @map$, "sarab";
    set @x, 83;
    set @y, 101;
    goto L_teleportar;

L_menuBatalha:
    mes "";
    mes "[Teleportadora Bellatrix]";
    mes "\"Também posso te levar direto para locais de batalha. Mas só para os locais que eu considerar seguro para você.";
    mes "Para onde gostaria de ir?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Pântano lvl 80 (2.000 GP)",   L_pantano,
        "Cemitério lvl 90 (5.000 GP)", L_cemiterio,
        "Melhor não arriscar!",        L_fechar;

L_pantano:
    if (BaseLevel < 80) goto L_semLvl;

    if (Zeny < 2000) goto L_semGrana;
    set Zeny, Zeny - 2000;

    set @map$, "org026";
    set @x, 36;
    set @y, 52;
    goto L_teleportar;

L_cemiterio:
    if (BaseLevel < 90) goto L_semLvl;

    if (Zeny < 5000) goto L_semGrana;
    set Zeny, Zeny - 5000;

    set @map$, "org027";
    set @x, 71;
    set @y, 42;
    goto L_teleportar;

L_teleportar:
    mes "";
    mes "[Teleportadora Bellatrix]";
    mes "\"Boa viagem.\"";
    close2;
    goto L_levitar;

L_levitar:
    fakenpcname "Teleportadora#hali", "Teleportadora#hali", 331;
    specialeffect2 310;
    addtimer 2800, "Teleportadora#hali::OnTimer";
    set @timer_running, 1;
    end;

L_semLvl:
    mes "";
    mes "[Teleportadora Bellatrix]";
    mes "\"Me desculpe, mas este não me parece um local seguro para você.\"";
    close;

L_semGrana:
    mes "";
    mes "[Teleportadora Bellatrix]";
    mes "\"Me desculpe, mas você não tem dinheiro suficiente para esta viagem.\"";
    close;

L_novoJogador:
    mes "[Teleportadora Noah]";
    mes "\"Sinto muito, mas ainda você não pode beneficiar dos meus serviços.\"";
    close;

L_fechar:
    close;

OnTimer:
    warp @map$, @x, @y;
    fakenpcname "Teleportadora#hali", "Teleportadora#hali", 330;
    set @timer_running, 0;
    end;
}
