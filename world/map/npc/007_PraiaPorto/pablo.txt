// @autor Diogo_RBG - http://diogorbg.blogspot.com
// @desc Pescador que adora a vida mansa... mas por um descuido ele quase se deu mau!
// #quest <desc>, <getItens>, <delItens>, <extra>
// @quest 'Presente de Casamento', '1 facao', '1 WhiteRose', ''
// @quest 'Matar Escorpiões', '780 exp', '', 'Matar 30 Escorpiões'


007,20,44,0|script|Pablo|310,10,10
{
    if (#GRINGO) goto L_CF;

    // Testa se missão já foi feita:
    //if (QUEST_MASK1 & MASK1_PRAIA1)

    // Seta missão como feita:
    //set QUEST_MASK1, QUEST_MASK1|MASK1_PRAIA1;
    //set QUEST_praia, 0;

    if (QUEST_praia == 8) goto L_intro;
    if (QUEST_praia == 9) goto L_pabloFria;
    if (QUEST_praia == 10) goto L_trouxeRosa;
    if (QUEST_praia == 11) goto L_premioFacao;
    if (QUEST_praia == 12) && (QUEST_contamobs >= 1) goto L_matando;
    if (QUEST_praia == 12) goto L_pabloContente;
    if (QUEST_praia >= 13) goto L_pablofim;

    mes "[Pablo o pescador]";
    mes "\"Você conhece minha esposa Cornélia? Ela é uma flor de pessoa.\"";
    next;
    mes "[Pablo o pescador]";
    mes "\"Ela está muito preocupada. Mas, mesmo assim, não quer me falar.\"";
    next;
    mes "[Pablo o pescador]";
    mes "\"Você pode tentar ajudá-la nesta cabana ao lado? Ela costuma retrubuir bem as pessoas que a ajudam.\"";
    close;

L_intro:
    mes "[Pablo o pescador]";
    mes "\"Você me parece ser o guerreiro que minha esposa Cornélia ajudou... rss... ela te deu mesmo um Rolo de Macarrão como arma?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim. O mais curioso é que me ajudou muito!", L_Sim;

L_Sim:
    mes "";
    mes "[Pablo o pescador]";
    mes "\"Minha esposa tem um coração puro e admiro isso nela. Não foi a toa que me casei com ela.\"";
    next;
    mes "De repente Pablo faz uma cara completamente pálida.";
    next;
    mes "[Pablo o pescador]";
    mes "\"Oh meu Deus! Hoje é o nosso aniversário de casamento! Eu o esqueci completamente. E o que é pior: NÃO TENHO NENHUM PRESENTE!!!\"";
    goto L_menuFlor;

L_pabloFria:
    mes "[Pablo o pescador]";
    mes "\"Eu esqueci completamente meu aniversário de casamento com Cornélia... e agora?\"";
    goto L_menuFlor;

L_menuFlor:
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Porque você não dá uma rosa a ela?", L_Rosa;

L_Rosa:
    mes "";
    mes "[Pablo o pescador]";
    mes "\"É o que vou fazer... mas não posso sair correndo pra comprar uma rosa pra ela. Ela vai percerber que me esqueci do nosso aniversário de casamento. E ela vai ficar uma fera comigo.\"";
    set QUEST_praia, 9;
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Meus pêsames!",                        L_Pesames,
        "Eu posso comprar uma rosa para você.", L_comproFlor;

L_Pesames:
    mes "";
    mes "[Pablo o pescador]";
    mes "\"Sim é meu fim!\"";
    close;

L_comproFlor:
    mes "";
    mes "[Pablo o pescador]";
    mes "\"Se você fizer isso irá salvar minha pele. Este dinheiro irá ajudar a comprar uma bela rosa para minha esposa. Por favor não demore!\"";
    set Zeny, Zeny + 100;
    set QUEST_praia, 10;
    close;

L_trouxeRosa:
    mes "[Pablo o pescador]";
    mes "\"Você trouxe a rosa que te pedi?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Sim. Está na minha mochila.", L_verificaRosa,
        "Me esqueci de trazer",        L_Esqueci;

L_Esqueci:
    mes "";
    mes "[Pablo o pescador]";
    mes "Por favor compre uma rosa com o dinheiro que te dei... estou dependendo de seu favor.";
    close;

L_verificaRosa:
    if (countitem("RosaBranca")) goto L_rosaCerta;

    if (countitem("RosaAmarela")        ||
        countitem("RosaAzul")           ||
        countitem("RosaLaranja")        ||
        countitem("RosaPreta")          ||
        countitem("RosaRosa")           ||
        countitem("RosaVermelha")       ||
        countitem("RosaVermelhaEscura")
    ) goto L_rosaErrada;

    mes "";
    mes "[Pablo o pescador]";
    mes "\"Você não me trouxe nenhuma rosa. Compre uma rosa com o dinheiro que te dei. Por favor não demore... estou dependendo de seu favor.\"";
    close;

L_rosaErrada:
    mes "";
    mes "[Pablo o pescador]";
    mes "\"Deixe-me ver a rosa que você me trouxe.\"";
    next;
    mes "Pablo olha sua mochila e fica ainda mais aflito.";
    next;
    mes "[Pablo o pescador]";
    mes "\"Essa não! Você não me trouxe a rosa correta. Não posso dar uma rosa a minha esposa que não seja a [" + getitemlink("RosaBranca") + "], pois ela representa amor eterno.\"";
    close;

L_rosaCerta:
    mes "";
    mes "[Pablo o pescador]";
    mes "\"Você me trouxe uma rosa branca representando amor eterno... é a rosa perfeita. Muito obrigado... você salvou minha pele.\"";
    delitem "RosaBranca", 1;
    set QUEST_praia, 11;
    next;
    goto L_premioFacao;

L_premioFacao:
    getinventorylist;
    if ((@inventorylist_count + (countitem(3030) == 0)) > 100) goto L_cheio;

    mes "[Pablo o pescador]";
    mes "\"Voltando ao assunto do 'Rolo de Macarrão'... Definitivamente um Rolo de Macarrão não é uma arma. Este Facão é que é uma arma.\"";
    next;
    mes "[Pablo o pescador]";
    mes "\"Facão que agora é seu!\"";
    getitem 3030, 1;
    set QUEST_praia, 12;
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Oba... um Facão!", L_Surpreso;

L_Surpreso:
    mes "";
    mes "[Pablo o pescador]";
    mes "\"Como você me salvou de uma fria... o Facão era mínimo que eu poderia fazer por você.\"";
    close;

L_pabloContente:
    mes "[Pablo o pescador]";
    mes "\"Hehe... assim que eu me encontrar com minha esposa darei a rosa a ela! E devo tudo a você!\"";
    goto L_mate30;

L_cheio:
    mes "[Pablo o pescador]";
    mes "\"Pretendia lhe dar algo, mas parece que seu inventário está cheio. Volte mais tarde.\"";
    close;

//=====================================================================

L_mate30:
    next;
    mes "[Pablo o pescador]";
    mes "\"O que anda fazendo com o Facão que te dei?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Matando um monstro aqui e ali.", L_Mato;

L_Mato:
    mes "";
    mes "[Pablo o pescador]";
    mes "\"Porque você não mata Escorpiões? Aposto que iria aumentar e muito sua experiência.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Irei matá-los agora mesmo.", L_Fechar;

L_matando:
    next;
    mes "[Pablo o pescador]";
    mes "\"A propósito. Quantos Escopiões você já matou?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Já matei " + (QUEST_contamobs) + " Escorpiões.", L_ContaEscorpioes;

L_ContaEscorpioes:
    if (QUEST_contamobs >= 30) goto L_matou30;
    mes "[Pablo o pescador]";
    mes "\"Essa quantia é insuficiente. Você precisa matar pelo menos 30 Escorpiões se quiser aumentar sua experiência.\"";
    close;

L_matou30:
    mes "";
    mes "[Pablo o pescador]";
    mes "\"Como eu disse. Matar muitos Escorpiões iria ajudar a aumentar sua experiência.\"";
    next;
    mes "* Você ganhou 780 pontos de experiência.";
    next;
    mes "[Pablo o pescador]";
    mes "\"Agora eu preciso entregar esta rosa para a Cornélia... Com certeza ela vai adorar o presente!\"";
    getexp 780, 0;
    set QUEST_contamobs, 0;
    set QUEST_praia, 13;
    close;

L_pablofim:
    mes "[Pablo o pescador]";
    mes "\"Muito obrigado por tudo, " + strcharinfo(0) + ". Você salvou meu dia.\"";
    close;

L_Fechar:
    close;

L_CF:
	callfunc "#EnPablo";
	end;

OnTouch:
    if ((QUEST_praia >= 0 && QUEST_praia < 8) || QUEST_praia >= 13) end;
    emotion EMOTE_QUEST;
    end;
}
