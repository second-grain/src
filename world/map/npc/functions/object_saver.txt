function|script|objectSaver
{
    mes "[" + @npc$ + "]";
    mes "Você vê um báu que parece emanar uma energia familiar. O que você vai fazer?";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Vou tocar no báu.", L_salvar,
        "Nada!",             L_Return;

L_salvar:
    mes "";
    mes "[" + @npc$ + "]";
    mes "Ao tocar neste báu você se sente como se parte da sua alma fosse absorvida dentro dele.";
    savepoint @map$, @x, @y;
    return;

L_Return:
    return;
}
