function|script|SoulMenhir
{
    mes "[Menir da Alma]";
    mes "Uma aura mística envolve essa pedra. Você se sente misteriosamente atraído por ela.";
    mes "Algo lhe diz para tocá-la. O que você fazer?";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Tocar a pedra.",            L_tocar,
        "Tentar cortar a pedra.",    L_cortar,
        "Dar uma pancada na pedra.", L_bater,
        "Resistir.",                 L_return;

L_tocar:
    if (Menhir_Activated == 1) goto L_shortVersion;

    mes "";
    mes "[Menir da Alma]";
    mes "Você toca na misteriosa pedra. De alguma forma se sente quente e frio ao mesmo tempo.";
    next;

    mes "[Menir da Alma]";
    mes "De repente, uma estranha sensação flui através de você.";
    mes "Parece que sua alma deixa o seu corpo e se funde à pedra.";
    next;

    mes "[Menir da Alma]";
    mes "Tão de repente como o sentimento começou ele se vai.";
    mes "A estranha atração é afastada de um momento para o outro e seus sentimentos pela menir não são mais que os de uma pedra ordinária.";

    set Menhir_Activated, 1;
    goto L_save;

L_shortVersion:
    mes "";
    mes "[Menir da Alma]";
    mes "Uma estranha sensação flui através de você. Parece que sua alma deixa o seu corpo e se funde à pedra.";
    mes "Tão de repente como o sentimento começou ele se vai.";
    goto L_save;

L_save:
    savepoint @map$, @x, @y;
    goto L_return;

L_cortar:
    if (@cortarMenir) goto L_naoCortar;
    mes "";
    mes "[Menir da Alma]";
    mes "Você empunha sua espada e golpeia a pedra com toda a sua força...";
    next;
    specialeffect 28;
    set @cortarMenir, 1;
    mes "[Menir da Alma]";
    mes "Faíscas voam por todos os lados, mas nada acontece com a pedra. Ela é tão resistente quanto o ferro.";
    return;

L_naoCortar:
    mes "";
    mes "[Menir da Alma]";
    mes "Assim você vai danificar a sua espada... melhor pensar em outra coisa.";
    return;

L_bater:
    if (TICK_MenirControle > gettimetick(2)) goto L_nadaAcontece;
    set TICK_MenirControle, gettimetick(2) + (60 * 60 * 24);
    mes "";
    mes "[Menir da Alma]";
    mes "Você bate na pedra com a ponta do cabo de sua espada e um pequeno fragmento se solta.";
    specialeffect 26;
    next;
    if (!TICK_MenirControle) goto L_energizado;
    mes "[Menir da Alma]";
    mes "O fragmento não possui qualquer energia... mas resolve guardá-lo para ver o que acontece.";
    getitem "FragmentoMenir", 1;
    return;

L_energizado:
    mes "[Menir da Alma]";
    mes "Assim como a menir, o fragmento também emana uma energia mística.";
    mes "O que será que isto pode significar?";
    getitem "FragmentoEnergizado", 1;
    return;

L_nadaAcontece:
    mes "";
    mes "[Menir da Alma]";
    mes "Você bate na pedra com a ponta do cabo de sua espada, mas nada acontece...";
    message strcharinfo(0), "Talvez eu precise esperar um pouco antes de tentar novamente.";
    return;

L_return:
    return;
}
