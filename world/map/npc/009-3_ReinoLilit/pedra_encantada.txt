// Reino de Lilit (2)
009-3,166,52,0|script|Pedra Encantada#009-3|400
{
    if (QUEST_pedraEncantada == 3) goto L_teleporte;

    mes "[Pedra Encantada]";
    mes "Ao tocar na Pedra Encantada você sente uma enorme energia fluir através de todo o seu corpo.";
    next;
    if (QUEST_pedraEncantada == 1) goto L_primeiroTeleporte;
    set QUEST_pedraEncantada, 2;
    mes "[Pedra Encantada]";
    mes "Mas essa energia vai perdendo força e nada acontece.";
    close;

L_primeiroTeleporte:
    set QUEST_pedraEncantada, 3;
    mes "[Pedra Encantada]";
    mes "Então você se lembra de ter sentido essa mesma energia ao tocar em outra pedra.";
    next;
    warp "009", 81, 77;
    mes "[Pedra Encantada]";
    mes "Dê repente você se sente sugado por essa energia... e quando retoma sua consciência está junto da outra pedra.";
    mes "É como se a pedra pudesse te levar ao encontro de seu pensamento.";
    close;

L_teleporte:
    mes "[Pedra Encantada]";
    mes "Ao tocar na Pedra Encantada e sentir a energia fluir você mentaliza a Pedra Encantada da ponte.";
    next;
    warp "009", 81, 77;
    mes "[Pedra Encantada]";
    mes "A energia toma conta de seu corpo e em instantes você é levado até lá.";
    close;
}
