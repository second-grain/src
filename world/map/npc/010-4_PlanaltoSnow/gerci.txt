// Autor:      Xtreem
// Descrição:  Npc Gerci guarda da fonte magica.


010-4,51,110,0|script|Gerci|150
{
    if (!debug) goto L_semlevel; // A quest parece incompleta.
    if (QUEST_gerci == 0 && BaseLevel < 90) goto L_semlevel;
    if (QUEST_gerci == 1 ) goto L_ok;

    mes "[Gerci]";
    mes "\"Olá, eu me chamo Gerci e sou guardião desta fonte.\"";
    next;
    mes "[Gerci]";
    mes "\"Minha familia ja guarda esta fonte há muitas gerações.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Que bom. Até mais",                 L_xau,
        "O que esta fonte tem de especial?", L_falarmais;

L_semlevel:
    mes "[Gerci]";
    mes "\"O que faz aqui criança?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes " ...";
    next;
    mes "[Gerci]";
    mes "\"Suma daqui agora. Sua vida esta em perigo.\"";
    close;
        
L_xau:
    mes "";
    mes "[Gerci]";
    mes "Até mais antão.";
    close;

L_falarmais:
    mes "";
    mes "[Gerci]";
    mes "\"Eita isso é uma estoria muito longa.\"";
    next;
    mes "[Gerci]";
    mes "\"Va até a biblioteca para saber mais.\"";
    next;
    mes "[Gerci]";
    mes "\"Pois eu estou muito ocupado não tenho tempo para te contar tudo agora.\"";
    next;
    mes "[Gerci]";
    mes "\"O que eu posso dizer é que a deus Freya fez uma magia nesta fonte há muito tempo.\"";
    next;
    mes "[Gerci]";
    mes "\"Até mais.\"";
    set QUEST_gerci, 1;
    close;

L_ok:
    mes "[Gerci]";
    mes "\"Olá. Ainda estou ocupado até mais.\"";
    close;
}
