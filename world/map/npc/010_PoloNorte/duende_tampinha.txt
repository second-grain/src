// @author  http://diogorbg.blogspot.com/
// @desc    O Tampinha explica como utilizar os sinos.


010,166,113,0|script|Duende Tampinha|321
{
    mes "[Duende Tampinha]";
    mes "\"Papai Noel pediu para que eu tocasse músicas de natal, mas não estou me lembrando como se toca!\"";
    next;
    menu
        "Também não sei como se toca.", L_NaoSei,
        "Posso tocar uma música?", L_possoTocar;

L_NaoSei:
    mes "";
    mes "[Duende Tampinha]";
    mes "\"Estou perdido!\"";
    close;

L_possoTocar:
    mes "";
    mes "[Duende Tampinha]";
    mes "\"É claro que pode! Basta ficar bem perto dos sinos e dar uma badalada em cada um.\"";
    close;
}
