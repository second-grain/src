// Autor:         Jesusalva <cpntb1@ymail.com>
// Revisão:       Scall
// Função:        Reiniciar a classe. Futuramente, quest de escudo.
// Preço:         1× Moeda do Rei, 1.000.000 GP.
//
// Notas:         Um GM lvl >=80 pode dar gratuitade ao reset.
//                Isso é particularmente útil em update de tiers.


005-3,24,142,0|script|Cavaleiro Britânico|576
{
    //getskilllist;
    set @Classes, 0;
    if (getskilllv(400) >= 1) set @Classes, @Classes + 1;
    if (getskilllv(401) >= 1) set @Classes, @Classes + 1;
    if (getskilllv(402) >= 1) set @Classes, @Classes + 1;
    if (getskilllv(403) >= 1) set @Classes, @Classes + 1;
    if (getskilllv(404) >= 1) set @Classes, @Classes + 1;
    if (getskilllv(405) >= 1) set @Classes, @Classes + 1;

    mes "[Cavaleiro Britânico]";
    mes "\"Olá, sou um poderoso cavaleiro da famosa Távola Quadrada. Porém agora estou de férias aqui na Ilha Fortaleza.\"";
    next;
    if (getgmlevel() >= 80) goto L_resetGM;
    if (@Classes == 0) goto L_sem_classe;
    if (@Classes >= 1) goto L_com_classe;
    close;

L_com_classe:
    mes "[Cavaleiro Britânico]";
    if (@Classes == 1) mes "\"Você faz parte da seguinte classe:\"";
    if (@Classes >= 2) mes "\"Estou vendo que você já possui " + @Classes + " classes, sendo elas:\"";

    set @Classe, 400;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    set @Classe, 401;
    callfunc "GerarNomeDaPatente";

    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    set @Classe, 402;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    set @Classe, 403;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    set @Classe, 404;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    set @Classe, 405;
    callfunc "GerarNomeDaPatente";
    if (@NomeDaPatente$ != "") mes " * " + @NomeDaPatente$;

    next;
    mes "[Cavaleiro Britânico]";
    mes "\"Eu posso remover seus vinculos com a(s) classe(s) anteriormente mencionada(s). E naturalmente não é barato uma mudança dessas.\"";
    next;
    if ($GRATIS_MUDARCLASSE >= 1) goto L_MudarGratis;

    mes "[Cavaleiro Britânico]";
    mes "\"Eu cobro uma [" + getitemlink("MoedaDoRei") + "] e 100.000 de GP por classe para rescindir sua afiliação. Todas as classes serão perdidas.";
    mes "Também terá que devolver o Grimorium de sua(s) classe(s).\"";
    menu
        "Estou muito contente com minha(s) classe(s).", L_close,
        "Reinicie minha(s) classe(s)!",                 L_Pagamento;

L_MudarGratis:
    mes "[Cavaleiro Britânico]";
    mes "\"Felizmente porém, o Castelo Imperial estará pagando pelas mudanças de classe por um curto tempo limitado.\"";
    mes "[" + strcharinfo(0) + "]";
    menu
        "Estou muito contente com minha(s) classe(s).", L_close,
        "Reinicie minha(s) classe(s)!",                 L_NoPaycheck;

L_NoPaycheck:
    set $GRATIS_MUDARCLASSE, $GRATIS_MUDARCLASSE+1;
    goto L_Pronto;

L_Pagamento:
    if (PAGOU_RESETCLASSE == 1) goto L_Pronto; // Já pagou o reset porém esqueceu o Grimorium
    if (countitem("MoedaDoRei") < 1 || (Zeny < (100000 * @Classes))) goto L_Insuficiente;
    delitem "MoedaDoRei", 1;
    set Zeny, Zeny - (100000 * @Classes);
    goto L_Pronto;

L_Pronto:
   // No caso de algo der errado agora, não precisa pagar novamente.
    set PAGOU_RESETCLASSE, 1;

    // Inicialização das variaveis.
    set @Cavaleiro, 0;

    set @Druida, 0;
    set @Mago, 0;
    set @Ninja, 0;
    set @Paladino, 0;
    set @Necromante, 0;
    // Precisamos confiscar o Grimorium também...
    if (getskilllv(400) >= 1) set @Cavaleiro,  1;
    if (getskilllv(401) >= 1) set @Druida,     1;
    if (getskilllv(402) >= 1) set @Mago,       1;
    if (getskilllv(403) >= 1) set @Ninja,      1;
    if (getskilllv(404) >= 1) set @Paladino,   1;
    if (getskilllv(405) >= 1) set @Necromante, 1;

    if (@Cavaleiro  == 1 && countitem("GrimoriumAzul")    < 1) goto L_SemGrimorium;
    if (@Druida     == 1 && countitem("GrimoriumVerde")   < 1) goto L_SemGrimorium;
    if (@Mago       == 1 && countitem("GrimoriumPurpura") < 1) goto L_SemGrimorium;
    if (@Ninja      == 1 && countitem("GrimoriumRubro")   < 1) goto L_SemGrimorium;
    if (@Paladino   == 1 && countitem("GrimoriumBranco")  < 1) goto L_SemGrimorium;
    if (@Necromante == 1 && countitem("GrimoriumNegro")   < 1) goto L_SemGrimorium;

    if (@Cavaleiro  == 1) delitem "GrimoriumAzul",    1;
    if (@Druida     == 1) delitem "GrimoriumVerde",   1;
    if (@Mago       == 1) delitem "GrimoriumPurpura", 1;
    if (@Ninja      == 1) delitem "GrimoriumRubro",   1;
    if (@Paladino   == 1) delitem "GrimoriumBranco",  1;
    if (@Necromante == 1) delitem "GrimoriumNegro",   1;

    // Agora eliminamos as skills.
    setskill SKILL_CAVALEIRO, 0;
    setskill SKILL_DRUIDA, 0;
    setskill SKILL_MAGO, 0;
    setskill SKILL_NINJA, 0;
    setskill SKILL_PALADINO, 0;
    setskill SKILL_NECROMANTE, 0;
    set QUEST_Classe, 2;
    set ELEVARTIER, 0;
    set PAGOU_RESETCLASSE, 0; // Não esquecer de que no próximo reset tem que pagar de novo!!

    mes "[Cavaleiro Britânico]";
    mes "\"Você não faz mais parte de nenhuma classe. Não tenho mais nada para falar contigo. Andando.\"";
    close;

L_Insuficiente:
    mes "[Cavaleiro Britânico]";
    mes "\"Eu sei que o preço é alto. Porém você não vai querer ser considerado um traidor e desertor, certo? A classe inteira iria te perseguir até a morte.";
    mes "É uma Moeda do Rei como prova, e 100.000 GP por cada classe que você estiver abandonando.\"";
    close;

L_SemGrimorium:
    mes "[Cavaleiro Britânico]";
    mes "\"Eu preciso do grimorium de sua(s) classe(s). Sem ele, não posso efetivar o desvinculo. Vou manter o pagamento e anotar em uma agenda. Próxima vez que vier aqui traga o Grimorium, e eu irei remover sua classe.\"";
    close;

L_sem_classe:
    mes "[Cavaleiro Britânico]";
    mes "\"De momento estou a serviço dos seis Mestres de Classe. Talvez no futuro você consiga uma classe, mas não tenha tanta esperança assim.\"";
    close;


L_resetGM:
    mes "[Cavaleiro Britânico]";
    mes "\"Vossa alteza pode aqui trocar a classe ou garantir gratuitade a todos. Até o momento o governo pagou " + $GRATIS_MUDARCLASSE + " reset(s) de classe.\"";
    menu
        "Apague o registro de pagamento, dê gratuitade.",      L_GovPagar,
        "Apague o registro de pagamento. Todos paguem o seu.", L_PlayPagar,
        "Eu quero mudar minha classe!",                        L_Pronto,
        "Obrigado, cavaleiro. Dispensado.",                    L_close;

L_GovPagar:
    set $GRATIS_MUDARCLASSE, 1;
    mes "[Cavaleiro Britânico]";
    mes "\"O governo agora está dando gratuitade no pagamento de mudança de classe.\"";
    next;
    goto L_resetGM;

L_PlayPagar:
    set $GRATIS_MUDARCLASSE, 0;
    mes "[Cavaleiro Britânico]";
    mes "\"Agora cada jogador precisa pagar para trocar de classe, como deveria ser.\"";
    next;
    goto L_resetGM;

L_close:
    mes "[Cavaleiro Britânico]";
    mes "\"Treine mais, aprenda mais. Seja bem sucedido, e até mais.\""; // A virgula após 'sucedido' é um recurso sonoro.
    close;

OnTouch:
    if (BaseLevel < 40 || QUEST_Classe <= 3) end;
    emotion EMOTE_QUEST; // Emote de quest (afiliado a uma classe)
    end;
}
