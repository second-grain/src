function|script|#En-Enfermeira
{
    if (Sex > 1) end;
    if (QUEST_SetReino == 1) goto L_SugereSaida;

    mes "[Nurse]";
    mes "= D \" hello youngling. Glad you woke up! \"";
    next;
    mes "[Nurse]";
    mes "=) \" Because of the blow to the head, you have been lying for three days in this infirmary.\"";
    next;
    mes "[Nurse]";
    mes "= O \" What happened? \"";
    next;
    if (Sex == 0) mes "[Girl]";
    if (Sex == 1) mes "[Boy]";
    mes "(X_x) \" I can not say. I do not remember anything. What a headache! Who am I? Where I am?\"";
    next;
    mes "[Nurse]";
    mes "= O \" I imagine the blow to the head was too strong. \"";
    next;
    mes "[Nurse]";
    mes "=) \" Good! For now I'll call you "+ strcharinfo(0) +" \"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "(-_-) \" I like this name! \"";
    next;
    mes "[Nurse]";
    mes "= O \" You were brought unconscious by two residents. I had to hide you from the guards in a very separate room to avoid being taken to wake you up and interrogate you.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "('-') \" Thanks for this! \"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "(-_-) \" I just remember the realm where I came from. \"";
    next;
    goto L_DizReino;

L_DizReino:
    // Para evitar que os novos jogadores nasçam todos no mesmo lugar
    set @n, rand(2);  
    if (@n == 0) goto L_HalicarnazoPrimeiro;
    goto L_FrozPrimeiro;

L_HalicarnazoPrimeiro:
    mes "[" + strcharinfo(0) + "]";
    menu 
        "I came from Halicarnazo!", L_VariaveisReino,
        "I came from Froz!",        L_VariaveisReino;

L_FrozPrimeiro:
    set @Froz, 1;
    mes "[" + strcharinfo(0) + "]";
    menu 
        "I came from Froz!",        L_VariaveisReino,
        "I came from Halicarnazo!", L_VariaveisReino;

L_VariaveisReino:
    // 2 == Halicarnazo, 3 == Froz
    if (@menu == 1) set Reino, 2 + @Froz;
    if (@menu == 1) savepoint "halicarnazo", 84, 63;
    if (@menu == 2) set Reino, 3 - @Froz;
    if (@menu == 2) savepoint "froz", 136, 29;
    goto L_Continua;

L_Continua:
    set QUEST_SetReino, 1;
    if (Reino == 0) set @NomeReino$, "lugares desconhecidos";
    if (Reino == 1) set @NomeReino$, "Bhramir";
    if (Reino == 2) set @NomeReino$, "Halicarnazo";
    if (Reino == 3) set @NomeReino$, "Froz";

    mes "";
    mes "[Nurse]";
    mes "= D \" So you are not a spy since we are in "+ @NomeReino$ +". \"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "= D \" How lucky! \"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "= (\" But unfortunately, I just remember this. \"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "= O \" In any case, I have to go for a walk to try to find out about me. \"";
    next;
    set CAPTS, 0;
    set STORY_CAPT0, 1;
    mes "[" + strcharinfo(0) + "]";
    mes "=) \" Thank you for everything! \"";
    next;
    mes "[Nurse]";
    mes "= O \" Okay! I wish you luck to find your memories! \"";
    close;

L_SugereSaida:
    if (getgmlevel() >= 80) goto L_MenuGM;
    mes "[Nurse]";
    mes "= O \" Okay! I wish you luck to find your memories! \"";
    close;

L_MenuGM:
    mes "[Nurse]";
    mes "= D \" Hello! My "+ @LadyLord$ + "! \" ";
    next;
    mes "[Nurse]";
    mes "= D \" What can I do for you? \"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Apagarei sua memória!", L_DebugQuest,
        "Reiniciarei os Bonés!", L_DebugCore,
        "Nada!",                 L_Nada;

L_Nada:
    mes "";
    mes "[Nurse]";
    mes "= D \" I hope to be useful next time! \"";
    close;

L_DebugQuest:
    set QUEST_SetReino, 0;
    set Reino, 0;
    mes "";
    mes "[Nurse]";
    mes "(X.x) Frizzzzz!";
    close;

L_DebugCore:
    set $ALLPLAYER, 1;
    mes "";
    mes "[Second Grain: ManaBird]";
    mes "(X.x) Frizzzzz!";
    close;
}
