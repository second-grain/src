// Autor:      Diogo_RBG - http://diogorbg.blogspot.com/
// Descrição:  Crozué é um alquimista especialista em capturar a alma de equipamentos de batalha.
//
// É muito importante manter esta linha comentada... ia ser um desastre isto no jogo. Descomente epenas para testes.
// 005,132,60,0    shop    shop    153,631:1,742:1,3283:1,579:1,3268:1,3055:1,3280:1,3056:1,3271:1,3053:1,3274:1,3054:1,3277:1


005,135,59,0|script|Alquimista Crozué|351
{
    if (@introCrozue == 1) goto L_menu;
    set @introCrozue, 1;

    mes "[Alquimista Crozué]";
    mes "\"Olá, me chamo Crozué... alquimista especialista em equipamentos de batalha.";
    mes "Gostaria de reduzir ou restaurar a energia da alma de sua espada hoje?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "¬.¬ De jeito nenhum.",                L_Tchau,
        "O.o Isso não faz nenhum sentido.",    L_explicar,
        "Sim... estou atrás de seus serviços", L_menu,
        "Hoje não... obrigad" + @fm$ + ".",    L_Tchau;

L_Tchau:
    mes "";
    mes "[Alquimista Crozué]";
    mes "\"Ok... me procure quando precisar de meus serviços.\"";
    close;

L_explicar:
    mes "";
    mes "[Alquimista Crozué]";
    mes "\"É normal a sua perplexidade. A maioria dos grandes ferreiros e alquimistas deste mundo também não tem conhecimento de minha técnica.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Me explique como você poderia retirar a alma de minha espada e porque eu estaria interessado em fazer isso.\"";
    next;
    mes "[Alquimista Crozué]";
    mes "\"Todos querem ser poderosos e procuram por poder. Mas se esquecem que devem ser ainda mais poderosos que o poder que procuram... precisam ter o domínio do poder para não serem dominados por ele.\"";
    next;
    mes "[Alquimista Crozué]";
    mes "\"Não é raro se deparar com espadas lendárias de imenso poder e não ter força o suficiente para empunhá-la.";
    mes "Mas se parte desta imensidão de energia for extraída, existirá uma chance.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Quer dizer que você pode me ajudar a controlar o poder de armas poderosas! Isso é muito bom.\"";
    next;
    mes "[Alquimista Crozué]";
    mes "\"Sim, mas existe um porém. Reduzir o poder da arma significa deixá-la um pouco mais fraca.";
    mes "Mas não há com o que se preocupar... posso restaurar o poder da arma novamente... se bem que isto é mais complicado.\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Complicado quanto?\"";
    next;
    mes "[Alquimista Crozué]";
    mes "\"É preciso transferir o poder de um objeto realmente poderoso para a arma... e também é preciso sorte.\"";
    close;

L_menu:
    mes "";
    mes "[Alquimista Crozué]";
    mes "\"Quais de meus serviços você está interessado?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Restauração da energia da alma.", L_menuRestaurar,
        "Redução da energia da alma.",     L_menuReducao,
        "Nenhum, obrigad" + @fm$ + ".",    L_Tchau2;

L_Tchau2:
    mes "[Alquimista Crozué]";
    mes "\"Me procure sempre que precisar de meus serviços.\"";
    close;

//= REDUÇÃO DE NÍVEL ===================================================

L_menuReducao:
    mes "[Alquimista Crozué]";
    mes "\"A redução consiste em extrair parte da energia da alma acumulada na arma e transferi-la para um cristal.";
    mes "Não é irreversível, poderei restaurar esta energia depois, mas teremos dificuldades para fazer isto.";
    mes "Isto dependerá principalmente da arma.\"";
    next;
    mes "[Alquimista Crozué]";
    mes "\"Você gostaria de reduzir a energia de que arma?\"";
    menu
        "Não quero reduzir arma nenhuma.", L_menu,
        "* Rock Knife",                    L_rockKnife1,
        "* Espada dos Imortais",           L_espadaImortais1,
        "* Espada de Kurogane",            L_espadaKurogane1,
        "* Espada Miguel Arcanjo",         L_espadaArcanjo1,
        "* Espada de Sangue",              L_espadaDemoniaca1,
        "Não tenho nenhuma destas armas.", L_menu;

L_verificar1:
    set @cont, 0;
    if (countitem(@normal) > 0) set @cont, @cont + 1;
    if (countitem(@modif)  > 0) set @cont, @cont + 1;
    if (countitem(@modif + 1) > 0) set @cont, @cont + 1;
    if (countitem(@modif + 2) > 0) set @cont, @cont + 4;
    return;

L_verificar11:
    set @cont, 0;
    if (countitem(631) < 1) set @cont, -1; // cristalEscuro
    return;

L_selecionar1:
    set @old, 0;
    set @new, 0;
    if (countitem(@normal) > 0) set @old, @normal;
    if (countitem(@normal) > 0) set @new, @modif;
    if (countitem(@modif) > 0) set @old, @modif;
    if (countitem(@modif) > 0) set @new, @modif + 1;
    if (countitem(@modif + 1) > 0) set @old, @modif + 1;
    if (countitem(@modif + 1) > 0) set @new, @modif + 2;
    return;

L_reducao1:
    getinventorylist;
    if (@inventorylist_count == 100) goto L_cheio;
    callsub L_verificar1;
    if (@cont != 1 && @cont != 5) goto L_falasReducaoErro;
    callsub L_verificar11;
    if (@cont < 0) goto L_falasReducaoErro;
    mes "[Alquimista Crozué]";
    mes "\"Irei reduzir o nível e também a força da sua arma '" + @nome$ + "'.";
    mes "Deseja realmente fazer isto?\"";
    menu
        "Mudei de ideia",       L_menu,
        "Continue, por favor.", L_reducao11;

L_reducao11:
    mes "";
    mes "Crozué então começa a trabalhar em sua arma.";
    next;
    mes "Ele coloca uma mão sobre a outra e sem tocar sua arma a faz brilhar intensamente.";
    next;
    mes "O brilho é cada vez mais intenso... você mal pode olhar para ele.";
    next;
    mes "[Alquimista Crozué]";
    mes "\"A extração desta quantidade de energia da alma deve ser o suficiente.\"";
    next;
    getinventorylist;
    if (@inventorylist_count == 100) goto L_cheio;
    callsub L_verificar1;
    if (@cont != 1 && @cont != 5) goto L_falasReducaoErro;
    callsub L_verificar11;
    if (@cont < 0) goto L_falasReducaoErro;
    callsub L_selecionar1;
    delitem @old, 1;
    getitem @new, 1;
    delitem 631, 1;  // cristalEscuro
    getitem 3283, 1; // cristalAlma
    mes "Crozué faz um movimento e de repente aquela enorme quantidade de energia estoura e se espalha pelo ar.";
    next;
    mes "Um fenômeno brilhante e ensurdecedor.";
    next;
    mes "Felizmente Crozué pode armazenar uma grande parte desta energia em seu cristal.";
    next;
    mes "[Alquimista Crozué]";
    mes "\"Sim... funcionou!";
    mes "A redução foi executada com sucesso.\"";
    close;

L_falasReducaoErro:
    mes "[Alquimista Crozué]";
    if (@cont == -1) mes "\"Preciso de um [Cristal Escuro] para trabalhar. Ou a energia da arma será perdida.\"";
    if (@cont == 0) mes "\"Você não tem esta arma.\"";
    if ((@cont > 0 && @cont < 4) || @cont > 4)  mes "\"Você tem muitas armas. Só posso trabalhar em uma delas.\"";
    if (@cont == 4)  mes "\"Não posso fazer outra redução nesta arma.\"";
    close;

L_cheio:
    mes "[Alquimista Crozué]";
    mes "\"Seria interessante armazenar a energia da arma em algum objeto para poder restaurá-la depois.";
    mes "Mas você não tem espaço para isto :\\\"";
    close;

//----------------------------------------------------------------------

L_rockKnife1:
    set @nome$, "Rock Knife";
    set @normal, 579; //RockKnife
    set @modif, 3268; //RockKnife_1
    goto L_reducao1;

L_espadaImortais1:
    set @nome$, "Espada dos Imortais";
    set @normal, 3056; //EspadaImortal
    set @modif, 3271;  //EspadaImortal_1
    goto L_reducao1;

L_espadaKurogane1:
    set @nome$, "Espada de Kurogane";
    set @normal, 3055; //EspadaKurogane
    set @modif, 3280;  //EspadaKurogane_1
    goto L_reducao1;

L_espadaArcanjo1:
    set @nome$, "Espada Miguel Arcanjo";
    set @normal, 3053; //EspadaMiguelArcanjo
    set @modif, 3274;  //EspadaMiguelArcanjo_1
    goto L_reducao1;

L_espadaDemoniaca1:
    set @nome$, "Espada de Sangue";
    set @normal, 3054; //EspadaDemoniaca
    set @modif, 3277;  //EspadaDemoniaca_1
    goto L_reducao1;

//= RESTAURAÇÃO DE NÍVEL ===============================================

L_menuRestaurar:
    mes "";
    mes "[Alquimista Crozué]";
    mes "\"A restauração é um pouco complicada. Envolve técnica, muita energia e um pouco de sorte.";
    mes "Esta grande quantidade de energia pode ser conseguida através de um [Cristal de Energia da Alma].";
    mes "E para aumentar nossas chances de sucesso, é altamente recomendado o uso de um [Trevo de Quatro Folhas]... é melhor garantir né :P\"";
    next;
    mes "[Alquimista Crozué]";
    mes "\"Você gostaria de restaurar a energia de que arma?\"";
    menu
        "Não quero restaurar nenhuma arma.", L_menu,
        "* Rock Knife",                      L_rockKnife2,
        "* Espada dos Imortais",             L_espadaImortais2,
        "* Espada de Kurogane",              L_espadaKurogane2,
        "* Espada Miguel Arcanjo",           L_espadaArcanjo2,
        "* Espada de Sangue",                L_espadaDemoniaca2,
        "Não tenho nenhuma destas armas.",   L_menu;

L_verificar2:
    set @cont, 0;
    if (countitem(@normal) > 0) set @cont, @cont + 4;
    if (countitem(@modif)  > 0) set @cont, @cont + 1;
    if (countitem(@modif + 1) > 0) set @cont, @cont + 1;
    if (countitem(@modif + 2) > 0) set @cont, @cont + 1;
    return;

L_verificar22:
    set @cont, 0;
    if (countitem(3283) < 1) set @cont, -1; // cristalAlma
    if (countitem(742)  < 1) set @cont, -2;  // trevodequatrofolhas
    return;

L_selecionar2:
    set @old, 0;
    set @new, 0;
    if (countitem(@modif) > 0) set @old, @modif;
    if (countitem(@modif) > 0) set @new, @normal;
    if (countitem(@modif + 1) > 0) set @old, @modif + 1;
    if (countitem(@modif + 1) > 0) set @new, @modif;
    if (countitem(@modif + 2) > 0) set @old, @modif + 2;
    if (countitem(@modif + 2) > 0) set @new, @modif + 1;
    return;

L_restauracao2:
    callsub L_verificar2;
    if (@cont != 1 && @cont != 5) goto L_falasRestauracaoErro;
    callsub L_verificar22;
    if (@cont < 0) goto L_falasRestauracaoErro;
    mes "[Alquimista Crozué]";
    mes "\"Irei restaurar o nível e também a força da sua arma '" + @nome$ + "'.";
    mes "Deseja realmente fazer isto?\"";
    menu
        "Mudei de ideia",       L_menu,
        "Continue, por favor.", L_restauracao22;

L_restauracao22:
    mes "Crozué então começa a trabalhar em sua arma.";
    mes "Ele coloca o trevo sobre a arma, o cristal na palma de sua mão e se concentra profundamente.";
    mes "Imediatamente o cristal e a arma começam a brilhar em sincronia.";
    next;
    mes "O brilho é muito intenso... a energia acumulada do cristal ilumina todo o ambiente.";
    mes "";
    mes "[Alquimista Crozué]";
    mes "\"Este é o momento... a energia se desprendeu.\"";
    next;
    callsub L_verificar2;
    if (@cont != 1 && @cont != 5) goto L_falasRestauracaoErro;
    callsub L_verificar22;
    if (@cont < 0) goto L_falasRestauracaoErro;
    callsub L_selecionar2;
    delitem @old, 1;
    getitem @new, 1;
    delitem 3283, 1; // cristalAlma
    delitem 742, 1;  // trevodequatrofolhas
    mes "Crozué dá um golpe e faz a pedra tocar a espada... exatamente onde se encontra o trevo.";
    mes "Então ocorre uma grande explosão de energia que se espalha pelo ar... mas com as mão sobre a arma, Crozué faz com que toda a energia seja absorvida pela arma.";
    mes "";
    mes "[Alquimista Crozué]";
    mes "\"Isto é magnífico... não me canso de apreciar este fenômeno!";
    mes "A restauração foi executada com sucesso.\"";
    close;

L_falasRestauracaoErro:
    mes "[Alquimista Crozué]";
    if (@cont == -1) mes "\"Preciso de um [Cristal de Energia da Alma] para trabalhar. Eu preciso da energia acumulada neste cristal.\"";
    if (@cont == -2) mes "\"Preciso de um [Trevo de Quatro Folhas] para trabalhar. Ou tudo pode se perder.\"";
    if (@cont == 0) mes "\"Você não tem esta arma.\"";
    if ((@cont > 0 && @cont < 4) || @cont > 4)  mes "\"Você tem muitas armas. Só posso trabalhar em uma delas.\"";
    if (@cont == 4)  mes "\"Não posso fazer outra restauração nesta arma.\"";
    close;

L_rockKnife2:
    set @nome$, "Rock Knife";
    set @normal, 579; //RockKnife
    set @modif, 3268; //RockKnife_1
    goto L_restauracao2;

L_espadaImortais2:
    set @nome$, "Espada dos Imortais";
    set @normal, 3056; //EspadaImortal
    set @modif, 3271;  //EspadaImortal_1
    goto L_restauracao2;

L_espadaKurogane2:
    set @nome$, "Espada de Kurogane";
    set @normal, 3055; //EspadaKurogane
    set @modif, 3280;  //EspadaKurogane_1
    goto L_restauracao2;

L_espadaArcanjo2:
    set @nome$, "Espada Miguel Arcanjo";
    set @normal, 3053; //EspadaMiguelArcanjo
    set @modif, 3274;  //EspadaMiguelArcanjo_1
    goto L_restauracao2;

L_espadaDemoniaca2:
    set @nome$, "Espada de Sangue";
    set @normal, 3054; //EspadaDemoniaca
    set @modif, 3277;  //EspadaDemoniaca_1
    goto L_restauracao2;
}
