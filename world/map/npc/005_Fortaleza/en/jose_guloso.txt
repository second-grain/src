// Nome:        Gordo José
// Descrição:   Este script contém as definições para 2 quests voltadas aos jogadores
//              iniciantes e intermediários.
// Requisitos:  15 gosmas de verme e uma tiny healing potion para os iniciantes, 20
//              cavesnake eggs e uma medium healing potion para os médios
// Recompensa:  2k para os iniciantes e 5k para os médios


function|script|#EnJoseGuloso
{
// mes ". :: Debug Mode ::.";
//    menu
//        "Continuar", -,
//        "Resetar", -;
//    if (@menu == 2) set QUEST_jose, 0;

    set @valor, 0;
    if (QUEST_jose == 0) goto L_inicion00b;
    if (QUEST_jose == 1) goto L_entregan00b;
    if (QUEST_jose == 2) goto L_indigestao;
    if (QUEST_jose == 3 && BaseLevel > 30) goto L_inicioMedio;
    if (QUEST_jose == 3) goto L_curado1;
    if (QUEST_jose == 4) goto L_entregaMedio;
    if (QUEST_jose == 5) goto L_indigestao;
//  if (QUEST_jose == 6 && QUEST_chanceler == 1) goto L_castelo; //será usado na quest do king's bow
    if (QUEST_jose == 6) goto L_vcmecurou; 
    end;

L_inicion00b: 
    mes "[José the Fatty]";
    mes "\"I'm so hungry that I could eat a container full of hot things .... \"";
    next;
    mes "["+ strcharinfo(0) +"]";
    menu
        "What a crazy guy!",                                                   L_Fechar,
        "Hey, but if you are hungry, why not ask for a food to the waitress?", L_razaon00b;

L_razaon00b:
    mes "";
    mes "[José the Fatty]";
    mes "\"You know what it is? They do not have what I want here!";
    mes "Hey !! Queque sabe knows you can not bring me the food I want!?";
    next;
    mes "["+ strcharinfo(0) +"]";
    menu
        "Of course, why not?",       L_requisitosn00b,
        "I'm not a fast-food delivery, ok?", L_Fechar;

L_requisitosn00b:
    mes "";
    mes "[José the Fatty]";
    mes "With his eyes shining with joy, Joseph tells him:";
    mes "\"Please bring me 15 ["+ getitemlink("GosmaDeVerme") +"]? I PAY WELL!\"";
    next;
    mes "["+ strcharinfo(0) +"]";
    menu 
        "Fall in the real Fatty, go hunt some worms and prepare your own snack!",                                     L_voceehmau,
        "You definitely have a different taste for food, but since you're going to pay ... I'll bring you the Worm's Goo.", L_Traz;

L_Traz:
    set QUEST_jose, 1;
    close;

L_voceehmau:
    mes "";
    mes "[José the Fatty]";
    mes "Apparently very sad, Joseph watches his eyes and says:";
    mes "\"Surely you do not understand what it's like to be chubby, I already had a beautiful body and today, food is all I have left. \"";
    close;

L_entregan00b: 
    mes "[José the Fatty]";
    mes "Babando like a hungry animal waiting for food, José asks:";
    mes "\"Where are my 15 ["+ getitemlink("GosmaDeVerme") +"]? \"";
    next;
    mes "["+ strcharinfo(0) +"]";
    menu
        "Here they are, enjoy!", L_trocan00b,
        "I still have not got them, sorry!", L_comopode;

L_comopode:
    mes "";
    mes "[José the Fatty]";
    mes "How can you be able to do this to me? I'm very hungry, please, I beg you, bring my food! \"";
    close; 

L_trocan00b:
    if (countitem("GosmaDeVerme") < 15) goto L_comopode; 
    mes "";
    mes "[José the Fatty]";
    mes "\"Wonderful! Now I can eat some! Thank you!";
    mes "Oh, I was forgetting, here's your money! \"";
    set Zeny, Zeny + 2000;
    delitem "GosmaDeVerme", 15;
    set QUEST_jose, 2;
    mes "";
    mes "You received 2,000 GPs.";
    close;

L_inicioMedio: 
    mes "[José the Fatty]";
    mes "\"I'm so hungry I could eat a house made of delicious things .... \"";
    next;
    mes "["+ strcharinfo(0) +"]";
    menu
        "Ah, talk to the Waitress",                                         L_razaomedio,
        "I huh. I better go away. Maybe that chubby man thinks I'm food!",  L_Fechar;

L_razaomedio:
    mes "";
    mes "[José the Fatty]";
    mes "\"I'm tired of staying here, these people do not have to sell the things I like to eat ... EI! You! Want to make good money? \"";
    next;
    mes "["+ strcharinfo(0) +"]";
    menu
        "Of course, who doesn't?", L_Aceita;

L_Aceita:
    mes "";
    mes "[José the Fatty]";
    mes "\"Bring me twenty snake eggs from the cave that I will pay you well. \"";
    next;
    mes "["+ strcharinfo(0) +"]";
    menu
        "I don't work for pennies!",         L_comopode,
        "As they say, paying well, what evil has?", L_OK;

L_OK:
    set QUEST_jose, 4;
    close;

L_entregaMedio: 
    mes "[José the Fatty]";
    mes "Drooling like a hungry animal waiting for food, José asks:";
    mes "\"Where are my cave snake eggs? \"";
    next;
    mes "["+ strcharinfo(0) +"]";
    menu
        "Here they are, enjoy!",          L_trocamedio,
        "I haven't got them yet, sorry!", L_comopode;

L_trocamedio:
    if (countitem("OvoDeCobraDaCaverna") < 20) goto L_comopode;
    mes "";
    mes "[José the Fatty]";
    mes "\"Wonderful! Now I can eat some more! Thank you!";
    mes "Oh, I was forgetting, here's your money! \"";
    set Zeny, Zeny + 5000; 
    delitem "OvoDeCobraDaCaverna", 20; 
    set QUEST_jose, 5;
    mes "";
    mes "You received 5,000 GP.";
    close;

L_indigestao:
    mes "[José the Fatty]";
    mes "You realize that José does not seem to be doing well";
    mes "\"AHHH, what indigestion, I need to get more at the food, damn it! \"";
    goto L_menuCura;

L_menuCura:
    mes "";
    mes "["+ strcharinfo(0) +"]";
    menu
        "Examine",                  L_examinar,
        "Give him something", L_darAlgo,
        "Let him rest",   L_Fechar;

L_examinar:
    mes "";
    mes "Joseph's face is pale and his body is shaking.";
    goto L_menuCura;

L_darAlgo:
    mes "";
    mes "["+ strcharinfo(0) +"]";
    menu
        "Poção de Cactus",                                        L_pcactus,
        "Poção Energética",                                       L_pener,
        "Poção de Concentração",                                  L_pcon,
        "Poção Mínima de cura",                                   L_mcura,
        "Poção Pequena de cura",                                  L_pcura,
        "Poção Média de cura",                                    L_medcura,
        "Poção Grande de cura",                                   L_gcura,
        "Ops! I don't think I have anything to help the fatty!",  L_menuCura;

L_pcactus:
    if (countitem("PocaoDeCactus") < 1) goto L_ops;
    delitem "PocaoDeCactus", 1;
    goto L_falhacura;

L_pener:
    if (countitem("PocaoEnergetica") < 1) goto L_ops;
    delitem "PocaoEnergetica", 1;
    goto L_falhacura;

L_pcon:
    if (countitem("PocaoDeConcentracao") < 1) goto L_ops;
    delitem "PocaoDeConcentracao", 1;
    goto L_falhacura;

L_mcura:
    if (countitem("PocaoMinimaDeCura") < 1) goto L_ops;
    delitem "PocaoMinimaDeCura", 1;
    set @valor, 250;
    if (QUEST_jose == 2) goto L_completan00b;
    goto L_falhacura;

L_pcura:
    if (countitem("PocaoPequenaDeCura") < 1) goto L_ops;
    delitem "PocaoPequenaDeCura", 1;
    set @valor, 500;
    if (QUEST_jose == 2) goto L_completan00b;
    goto L_falhacura;

L_medcura:
    if (countitem("PocaoMediaDeCura") < 1) goto L_ops;
    delitem "PocaoMediaDeCura", 1;
    set @valor, 1000;
    if (QUEST_jose == 2) goto L_completan00b;
    if (QUEST_jose == 5) goto L_completamedio;
    close;

L_gcura:
    if (countitem("PocaoGrandeDeCura") < 1) goto L_ops;
    delitem "PocaoGrandeDeCura", 1;
    set @valor, 2000;
    if (QUEST_jose == 2) goto L_completan00b;
    if (QUEST_jose == 5) goto L_completamedio;
    close;

L_ops:
    mes "";
    mes "When you look in your inventory, you realize that you do not have the potion you intended to give to Joseph.";
    mes "You instantly think you should have been careful.";
    goto L_menuCura;

L_falhacura:
    mes "";
    mes "[José the Fatty]";
    mes "With some difficulty he drinks the potion.";
    next;
    mes "After a loud belching, you realize that he has not improved.";
    goto L_menuCura;

L_completan00b:
    mes "";
    mes "[José the Fatty]";
    mes "With some difficulty he drinks the potion.";
    next;
    mes "After a loud belching, Jose seems to be very well!";
    set QUEST_jose, 3;
    next;
    goto L_curado1;

L_curado1:
    mes "[José the Fatty]";
    mes "\"Wow, thank you! I'm fine, see !? Maybe another time I'll ask you something else! \"";
    if (@valor) goto L_pagamentoCura;
    close;

L_pagamentoCura:
    next;
    mes "[José the Fatty]";
    mes "\"I feel so good, I'm going to give you another buck for helping me make that pain go away. \"";
    set Zeny, Zeny + @valor;
    mes "";
    mes "You received"+ @valor +"GP.";
    close;

L_completamedio:
    mes "[José the Fatty]";
    mes "With some difficulty he drinks the potion.";
    next;
    mes "After a loud belching, Jose seems to be very well!";
    set QUEST_jose, 6;
    next;
    goto L_vcmecurou;

L_vcmecurou:
    mes "[José the Fatty]";
    mes "\"I feel better now, thank you so much for helping me!";
    mes "From now on I'll eat less invasive things. \"";
    if (@valor) goto L_pagamentoCura;
    close;

L_Fechar:
    close;
}
