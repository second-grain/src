// @author   Sky
// @desc     Controle de entrada
// @comment  Lunovox: Alguem sabe me dizer para que serve este clube ou este é mais uma das traquitanas inuteis do tmw-br?


005,34,26,0|script|Guarda#ClubeBhramir|110
{
    mes "[GUARDA]";
    mes "\"Saia daqui! Você não tem estilo o suficiente para entrar neste local.\"";
    close;
}

005,33,25,0|script|#PlacaClubeBhramir|300
{
    mes "[PLACA]";
    mes "\"Clube Bhramir.\"";
    close;
}

005,32,25,0|script|#controleentrada|300,0,0
{
    if (getequipid(equip_head) == 2210
        || getequipid(equip_head) == 2211
        || getequipid(equip_head) == 2212
        || getequipid(equip_head) == 2213
        || getequipid(equip_head) == 2214
        || getequipid(equip_head) == 2215
        || getequipid(equip_head) == 2216
        || getequipid(equip_head) == 2217
        || getequipid(equip_head) == 2218
        || getequipid(equip_head) == 2219
        || getequipid(equip_head) == 2999
        || getequipid(equip_head) == 627
    ) goto L_PodeEntrar;

    set @Classes, 0;
    if (getskilllv(400) >= 1) set @Classes, @Classes + 1;
    if (getskilllv(401) >= 1) set @Classes, @Classes + 1;
    if (getskilllv(402) >= 1) set @Classes, @Classes + 1;
    if (getskilllv(403) >= 1) set @Classes, @Classes + 1;
    if (getskilllv(404) >= 1) set @Classes, @Classes + 1;
    if (getskilllv(405) >= 1) set @Classes, @Classes + 1;

    if (@Classes >= 1) goto L_PodeEntrar;

    warp "005", 32, 26;
    mes "[GUARDA]";
    mes "\"Saia daqui! Você não tem estilo o suficiente para entrar neste local.\"";
    close;
    
L_PodeEntrar:
    mes "[GUARDA]";
    if (Sex == 1) mes "\"Bem-vindo, cavalheiro!\"";
    if (Sex == 0) mes "\"Bem-vinda, dama!\"";
    close2;
    warp "005-3", 39, 22;
    end;
}
