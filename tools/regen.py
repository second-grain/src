#!/usr/bin/env python
# -*- coding: utf-8 -*-
# “everyone hates PEP” - Jesusalva
import sys

# Let's start this simple, without using OS list

src=open("../"+sys.argv[1]+".txt", 'r')
mrc=open("xRegen.c", 'r') # translated copy from xDegen
nrc=open(sys.argv[1]+".txt", 'w')

gid=0
XMC=[]
for line in mrc:
	XMC.append(line)

print "\033[1mGerador de tradução, versão NÃO-USE por Jesusalva\033[0m"
tes0=0
test=0
tes2=0

# Before all, standard fixes!
for line in XMC:
	if tes0:
		print "Now: " +line
		print "Size: "+str(len(line))
		print "Char: "+line[(len(line)-2)]
		tes0-=1
	if line[(len(line)-2)] != ";":
		line=line[0:(len(line)-2)]
		line+='";\n'
		XMC[gid]=line
	gid+=1

gid=0
for line in XMC:
	if test:
		print "Rep: " + line

	line=line.replace('\\ "', '\\"')
	line=line.replace('\\ "', '\\"').replace('If', 'if').replace('Mes', 'mes').replace('Month', 'mes').replace('month', 'mes')
	line=line.replace(' "', '"').replace('" ', '"').replace(" (", "(").replace("@ ", "@").replace("$ ", "$").replace(" $", "$")
	line=line.replace('mes"', 'mes "')

	if test:
		print "New: " + line
		test-=1

	XMC[gid]=line
	gid+=1

gid=0
for line in src:
	if ('\tmes ' in line) or (' mes ' in line):
		nrc.write(XMC[gid])
		if tes2:
			print "WRT: " + XMC[gid]
			tes2-=1
		gid+=1
	else:
		nrc.write(line)

src.close()
mrc.close()
nrc.close()

print "Por favor se lembre de adequar o código original:"
print "    if (#GRINGO) goto L_CF;"
print "";
print "L_CF:\n\
	callfunc \"#En-"+sys.argv[1]+"\"; // or callsub?\n\
	end;"
print "\n\n"
print "Aonde o cabeçalho do novo arquivo será:"
print "function|script|#En-"+sys.argv[1]

